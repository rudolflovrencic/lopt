#ifndef RANDOM_HPP
#define RANDOM_HPP

#include "Configuration.hpp"

#include <McDeval.hpp>

namespace Random {

/** Returns a reference to a random fragment inside application configuration
 * object. */
const McDeval::Fragment& fragment(const McDeval::Configuration& app_config);

/** Returns a reference to a random result inside application configuration
 * object. */
const McDeval::Result& result(const McDeval::Configuration& app_config);

/** Returns a reference to a random component inside application configuration
 * object. */
const McDeval::Component& component(const McDeval::Configuration& app_config);

/** Returns a reference to a random server inside application configuration
 * object. */
const McDeval::Server& server(const McDeval::Configuration& app_config);

/** Returns a reference to a random resource inside application configuration
 * object. */
const McDeval::Resource& resource(const McDeval::Configuration& app_config);

/** Constructs a new random solution. */
McDeval::Solution solution(const McDeval::Configuration& app_config);

/** Returns a random boolean. */
bool boolean();

}

#endif
