#ifndef ALGORITHM_ECF_MUTATIONS_HPP
#define ALGORITHM_ECF_MUTATIONS_HPP

#include <McDeval.hpp>
#include <ECF.h>

#include "Configuration.hpp"

namespace ECF {

/** Base class for all layout mutation operators. Abstracts away all ECF types
 * for extending mutation operators. */
class LayoutMutationOperator : public MutationOp {
  protected:
    const McDeval::Configuration& app_config;

  public:
    explicit LayoutMutationOperator(const McDeval::Configuration& app_config);

    /** Function required by the ECF. Serves as a wrapper around mutation
     * function that accepts actual layout solution instead of generic gene
     * pointer. In other words, all this function does is communicate with ECF
     * and hence cannot be overridden.*/
    bool mutate(GenotypeP gene) final;

  private:
    /** Mutates a given solution. All mutation operators have to override this
     * function. */
    virtual void mutate(McDeval::Solution& sol) = 0;
};

/** Mutation operator that reassigns a random assignable object (fragment or
 * component) to a random server. */
class RandomAssignableToRandomServer : public LayoutMutationOperator {
  public:
    explicit RandomAssignableToRandomServer(
        const McDeval::Configuration& app_config);

    /** Reassigns randomly chosen assignable object (fragment or component) to a
     * randomly chosen server. */
    void mutate(McDeval::Solution& sol) override;
};

/** Mutation operator that completely randomizes the solution. */
class RandomSolution : public LayoutMutationOperator {
  public:
    explicit RandomSolution(const McDeval::Configuration& app_config);

    /** Completely randomizes the provided solution. */
    void mutate(McDeval::Solution& sol) override;
};

}

#endif
