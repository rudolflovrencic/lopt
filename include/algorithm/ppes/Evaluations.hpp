#ifndef ALGORITHM_PPES_EVALUATIONS_HPP
#define ALGORITHM_PPES_EVALUATIONS_HPP

#include <PPES.hpp>

#include "Configuration.hpp"

namespace PPES {

/** Wrapper for layout evaluators so that they can be used by Predator-Prey
 * algorithm. Handles all layout evaluators by leveraging polymorphism. */
class PredatorPreyEvaluator : public Objective<McDeval::Solution> {
  private:
    /** Application configuration reference needed for cloning. */
    const McDeval::Configuration& app_config;

    /** Evaluator type (required for cloning). */
    const McDeval::Evaluator::Type evaluator_type;

    /** Layout evaluator object created based on the provided name. */
    std::unique_ptr<McDeval::Evaluator> evaluator;

  public:
    PredatorPreyEvaluator(McDeval::Evaluator::Type evaluator_type,
                          const McDeval::Configuration& app_config);

  private:
    double evaluate(const McDeval::Solution& solution) const override;
    std::unique_ptr<Objective<McDeval::Solution>> clone() const override;
};

}

#endif
