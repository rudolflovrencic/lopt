#ifndef LOG_HPP
#define LOG_HPP

#include <fmt/color.h>
#include <fmt/ostream.h>

class Log {
  public:
#ifndef NLOG
    template<typename... T>
    static void normal(std::string_view format_string, const T&... args)
    {
        fmt::print(format_string, args...);
        fmt::print("\n");
    }

    template<typename... T>
    static void success(std::string_view format_string, const T&... args)
    {
        fmt::print(fg(fmt::color::green), format_string, args...);
        fmt::print("\n");
    }

    template<typename... T>
    static void warn(std::string_view format_string, const T&... args)
    {
        fmt::print(fg(fmt::color::yellow), format_string, args...);
        fmt::print("\n");
    }

    template<typename... T>
    static void error(std::string_view format_string, const T&... args)
    {
        fmt::print(stderr, fg(fmt::color::red), format_string, args...);
        fmt::print(stderr, "\n");
    }
#else
    template<typename... T>
    static void normal(std::string_view /*format_string*/, const T&... /*args*/)
    {}

    template<typename... T>
    static void success(std::string_view /*format_string*/,
                        const T&... /*args*/)
    {}

    template<typename... T>
    static void warn(std::string_view /*format_string*/, const T&... /*args*/)
    {}

    template<typename... T>
    static void error(std::string_view /*format_string*/, const T&... /*args*/)
    {}
#endif
};

#endif
