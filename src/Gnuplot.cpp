#include "Gnuplot.hpp"

Gnuplot::Gnuplot(bool persist) : pipe(nullptr)
{
    // Check if gnuplot can be opened (executable exists).
    if (system("which gnuplot > /dev/null 2>&1")) { // NOLINT
        throw GnuplotException(
            "Gnuplot process failed to open. Make sure gnuplot is installed "
            "and is present in system path.");
    }

    // Open gnuplot and pipe to it.
    pipe = popen(persist ? "gnuplot -persist" : "gnuplot", "w"); // NOLINT
    if (!pipe) { throw GnuplotException("Failed to open gnuplot pipe."); }
}

Gnuplot::~Gnuplot()
{
    if (pipe) { pclose(pipe); }
}

Gnuplot& Gnuplot::operator<<(const std::string& s)
{
    fputs(s.c_str(), pipe);
    return *this;
}

Gnuplot& Gnuplot::operator<<(char c)
{
    fputc(c, pipe);
    return *this;
}

Gnuplot::Gnuplot(Gnuplot&& other) noexcept : pipe(other.pipe)
{
    other.pipe = nullptr;
}

Gnuplot& Gnuplot::operator=(Gnuplot&& other) noexcept
{
    if (pipe) { pclose(pipe); }
    pipe       = other.pipe;
    other.pipe = nullptr;
    return *this;
}

void Gnuplot::flush()
{
    fflush(pipe);
}

GnuplotException::GnuplotException(std::string message) noexcept
    : message(std::move(message))
{}

const char* GnuplotException::what() const noexcept
{
    return message.c_str();
}
