#include "algorithm/ecf/ConfigurationFile.hpp"
#include "Configuration.hpp"

#include <rapidxml/rapidxml.hpp>
#include <rapidxml/rapidxml_print.hpp>

#include <cstring>
#include <fstream>

namespace {
rapidxml::xml_node<>* allocate_node(rapidxml::xml_document<>& doc,
                                    rapidxml::xml_node<>* parent,
                                    const char* name,
                                    const char* value = "");
void allocate_attribute(rapidxml::xml_document<>& doc,
                        rapidxml::xml_node<>* node,
                        const char* name,
                        const char* value);
void allocate_entry_node(rapidxml::xml_document<>& doc,
                         rapidxml::xml_node<>* parent,
                         const char* key,
                         const char* value);
}

namespace ECF {

void write_ecf_configuration_file(const char* filename,
                                  const AlgorithmConfiguration& alg_config)
{
    // Strings of algorithm configuration numbers. Their lifetime needs to be
    // extended to the end of this function so that is why they are here.
    std::string pop_size = std::to_string(alg_config.population_size.value());
    std::string gen_max  = std::to_string(alg_config.generation_max.value());
    std::string mut_rate = std::to_string(alg_config.mutation_rate.value());
    std::string log_freq = std::to_string(alg_config.log_frequency.value());

    rapidxml::xml_document<> doc;
    auto* e = doc.allocate_node(rapidxml::node_element, "ECF");

    {
        auto* a = allocate_node(doc, e, "Algorithm", "abx");
        {
            auto* sst = allocate_node(doc, a, "SteadyStateTournament");
            {
                allocate_entry_node(doc, sst, "tsize", "3");
            }
        }

        auto* g = allocate_node(doc, e, "Genotype");
        {
            allocate_node(doc, g, "LayoutGenotype");
        }

        auto* r = allocate_node(doc, e, "Registry");
        {
            allocate_entry_node(doc, r, "population.size", pop_size.c_str());
            allocate_entry_node(doc, r, "term.maxgen", gen_max.c_str());
            allocate_entry_node(doc, r, "mutation.indprob", mut_rate.c_str());
            allocate_entry_node(doc, r, "log.frequency", log_freq.c_str());
        }
    }
    doc.append_node(e);

    std::ofstream fstr(filename);
    fstr << doc;
    doc.clear();
}

}

namespace {

rapidxml::xml_node<>* allocate_node(rapidxml::xml_document<>& doc,
                                    rapidxml::xml_node<>* parent,
                                    const char* name,
                                    const char* value)
{
    if (std::strlen(name) == 0) {
        throw std::logic_error("Cannot create XML node with empty name.");
    }
    value   = std::strlen(value) ? value : nullptr;
    auto* n = doc.allocate_node(rapidxml::node_element, name, value);
    parent->append_node(n);
    return n;
}

void allocate_attribute(rapidxml::xml_document<>& doc,
                        rapidxml::xml_node<>* node,
                        const char* name,
                        const char* value)
{
    if (std::strlen(name) == 0) {
        throw std::logic_error("Cannot create XML attribute with empty name.");
    }
    if (std::strlen(value) == 0) {
        throw std::logic_error("Cannot create XML attribute with empty value.");
    }
    node->append_attribute(doc.allocate_attribute(name, value));
}

void allocate_entry_node(rapidxml::xml_document<>& doc,
                         rapidxml::xml_node<>* parent,
                         const char* key,
                         const char* value)
{
    if (std::strlen(key) == 0) {
        throw std::logic_error("Entry node cannot have an empty key.");
    }
    if (std::strlen(key) == 0) {
        throw std::logic_error("Entry node cannot have empty value.");
    }
    auto* en = allocate_node(doc, parent, "Entry", value);
    allocate_attribute(doc, en, "key", key);
}

}

