#include "algorithm/ecf/Crossovers.hpp"

#include "Random.hpp"
#include "algorithm/ecf/LayoutGenotype.hpp"

namespace ECF {

LayoutCrossoverOperator::LayoutCrossoverOperator(
    const McDeval::Configuration& app_config)
    : app_config(app_config)
{}

bool LayoutCrossoverOperator::mate(GenotypeP gen1,
                                   GenotypeP gen2,
                                   GenotypeP child)
{
    // Extract parent and child solutions from generic pointers.
    LayoutGenotypeP g1 = boost::dynamic_pointer_cast<LayoutGenotype>(gen1);
    const McDeval::Solution& p1 = g1->solution; // First parent.
    LayoutGenotypeP g2 = boost::dynamic_pointer_cast<LayoutGenotype>(gen2);
    const McDeval::Solution& p2 = g2->solution; // Second parent.
    LayoutGenotypeP child_p =
        boost::dynamic_pointer_cast<LayoutGenotype>(child);
    McDeval::Solution& ch = child_p->solution; // Child.
    mate(p1, p2, ch);

    return true;
}

EachPartFromRandomParent::EachPartFromRandomParent(
    const McDeval::Configuration& app_config)
    : LayoutCrossoverOperator(app_config)
{}

void EachPartFromRandomParent::mate(const McDeval::Solution& s1,
                                    const McDeval::Solution& s2,
                                    McDeval::Solution& child)
{
    // Get each fragment server randomly from first or the second parent.
    for (auto&& f : app_config.fragments) {
        auto&& s = Random::boolean() ? s1.get_server(f) : s2.get_server(f);
        child.set_server(f, s);
    }

    // Get each component server randomly from first or the second parent.
    for (auto&& c : app_config.components) {
        auto&& s = Random::boolean() ? s1.get_server(c) : s2.get_server(c);
        child.set_server(c, s);
    }
}

}
