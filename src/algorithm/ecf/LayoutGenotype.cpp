#include "algorithm/ecf/Crossovers.hpp"
#include "algorithm/ecf/LayoutGenotype.hpp"
#include "algorithm/ecf/Mutations.hpp"

#include "Random.hpp"

namespace ECF {
LayoutGenotype::LayoutGenotype(const McDeval::Configuration& app_config)
    : app_config(app_config)
{
    name_ = "LayoutGenotype";
}

LayoutGenotype* LayoutGenotype::copy()
{
    LayoutGenotype* newObject = new LayoutGenotype(*this); // NOLINT
    return newObject;
}

std::vector<CrossoverOpP> LayoutGenotype::getCrossoverOp()
{
    std::vector<CrossoverOpP> crx;
    crx.push_back(
        static_cast<CrossoverOpP>(new EachPartFromRandomParent(app_config)));
    return crx;
}

std::vector<MutationOpP> LayoutGenotype::getMutationOp()
{
    std::vector<MutationOpP> mut;
    mut.push_back(static_cast<MutationOpP>(
        new RandomAssignableToRandomServer(app_config)));
    mut.push_back(static_cast<MutationOpP>(new RandomSolution(app_config)));
    return mut;
}

bool LayoutGenotype::initialize(StateP /*state*/)
{
    solution = Random::solution(app_config);
    return true;
}

void LayoutGenotype::write(XMLNode& xMyGenotype)
{
    xMyGenotype = XMLNode::createXMLTopNode("LayoutGenotype");
    std::stringstream sValue;
    sValue << solution;
    xMyGenotype.addText(sValue.str().c_str());
}

void LayoutGenotype::read(XMLNode& /*node*/) {}

}
