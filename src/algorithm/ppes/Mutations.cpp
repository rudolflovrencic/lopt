#include "algorithm/ppes/Mutations.hpp"

#include "Random.hpp"

namespace PPES {

LayoutMutationOperator::LayoutMutationOperator(
    const std::string& name, const McDeval::Configuration& app_config)
    : Mutation(name), app_config(app_config)
{}

RandomSolution::RandomSolution(const McDeval::Configuration& app_config)
    : LayoutMutationOperator("Random Solution", app_config)
{}

void RandomSolution::mutate(const McDeval::Solution& /*src*/,
                            McDeval::Solution& dest) const
{
    dest = ::Random::solution(app_config);
}

RandomAssignableToRandomServer::RandomAssignableToRandomServer(
    const McDeval::Configuration& app_config)
    : LayoutMutationOperator("Random Assignable to Random Server", app_config)
{}

void RandomAssignableToRandomServer::mutate(const McDeval::Solution& src,
                                            McDeval::Solution& dest) const
{
    dest = src; // Copy source solution into destination.

    // Random server where assignable object will be reassigned.
    const auto& server = ::Random::server(app_config);

    bool fragment = ::Random::boolean();
    if (fragment) { // Random fragment should be reassigned.
        const auto& fragment = ::Random::fragment(app_config);
        dest.set_server(fragment, server);
    } else { // Random component should be reassigned.
        const auto& component = ::Random::component(app_config);
        dest.set_server(component, server);
    }
}

}
