#include "algorithm/ppes/PredatorPreyAlgorithm.hpp"

#include "algorithm/ppes/Evaluations.hpp"
#include "algorithm/ppes/Mutations.hpp"

#include "Random.hpp"

namespace PPES {

namespace {
/** Application configuration used for prey initialization. This is set when
 * Predator-Prey algorithm object is constructed. */
const McDeval::Configuration* app_config_p = nullptr;
}

PredatorPreyAlgorithm::PredatorPreyAlgorithm(
    const AlgorithmConfiguration& alg_config,
    const McDeval::Configuration& app_config)
    : MultiObjectiveAlgorithm(alg_config, app_config)
{
    // Makes sure all parameters required by this algorithm are set in the
    // algorithm configuration object. Note parameters common for all multi
    // objective algorithms are checked in the super class constructor, so here,
    // only parameters specific to this algorithm are checked.
    validate_parameters();

    app_config_p = &app_config; // Assign static application configuration.

    // Create evaluators (objectives) for Predator-Prey algorithm.
    objs.emplace_back(std::make_unique<PredatorPreyEvaluator>(
        alg_config.evaluators[0].evaluator_type, app_config));
    objs.emplace_back(std::make_unique<PredatorPreyEvaluator>(
        alg_config.evaluators[1].evaluator_type, app_config));

    // Create mutations for Predator-Prey algorithm.
    muts.emplace_back(std::make_unique<RandomSolution>(app_config));
    muts.emplace_back(
        std::make_unique<RandomAssignableToRandomServer>(app_config));

    // Initial plotting range.
    std::vector<std::pair<double, double>> range = {
        {alg_config.evaluators[0].plot_range.value()[0],
         alg_config.evaluators[0].plot_range.value()[1]},
        {alg_config.evaluators[1].plot_range.value()[0],
         alg_config.evaluators[1].plot_range.value()[1]}};

    // Create Predator-Prey algorithm configuration object.
    ppes_config = std::make_unique<Configuration>(
        alg_config.world_width.value(),
        alg_config.world_height.value(),
        alg_config.predators_per_objective.value(),
        alg_config.generation_max.value(),
        alg_config.log_frequency.value(),
        alg_config.plot_frequency.value(),
        2, // Logging precision.
        alg_config.pause_after_plot.value(),
        range);

    // Create Predator-Prey algorithm object.
    algorithm = std::make_unique<Algorithm<McDeval::Solution>>(
        *ppes_config, objs, muts);
}

std::vector<McDeval::Solution> PredatorPreyAlgorithm::run()
{
    algorithm->start();
    return algorithm->get_nondominated_solutions();
}

std::vector<std::vector<McDeval::Risk>>
PredatorPreyAlgorithm::get_nondominated_solution_risks() const
{
    // Get nondominated solutions of the last algorithm run.
    std::vector<McDeval::Solution> nds =
        algorithm->get_nondominated_solutions();

    // Risks for each pareto solution so total number of risks is number of
    // pareto solutions times number of objectives.
    std::vector<std::vector<McDeval::Risk>> risks;
    risks.reserve(nds.size()); // Reserve space for risks of pareto solutions.

    for (const McDeval::Solution& s : nds) {
        std::vector<McDeval::Risk> r; // Solution risks for each objective.
        r.reserve(objs.size()); // Reserve space for risk of each objective.

        // Evaluate solution with each objective function.
        for (auto&& obj : objs) { r.emplace_back(obj->evaluate(s)); }

        // Store risks of current solution into return vector of risks.
        risks.emplace_back(std::move(r));
    }

    return risks;
}

void PredatorPreyAlgorithm::validate_parameters() const
{
    if (!alg_config.world_width.has_value()) { // World width.
        throw_missing_parameter_error("world_width");
    }

    if (!alg_config.world_height.has_value()) { // World height.
        throw_missing_parameter_error("world_height");
    }

    if (!alg_config.predators_per_objective.has_value()) { // Predators.
        throw_missing_parameter_error("predators_per_objective");
    }
}

/** Prey default constructor. This constructs a random solution and serves for
 * initial population initialization. Required by PPES library. */
template<>
Prey<McDeval::Solution>::Prey()
{
    x = ::Random::solution(*app_config_p);
}

}
