#include "algorithm/openga/MultiObjectiveGeneticAlgorithm.hpp"

#include "MultiObjectivePlotter.hpp"
#include "Random.hpp"
#include "Log.hpp"

namespace OpenGA {

namespace {
void init_genes(McDeval::Solution& s, const std::function<double(void)>& rnd01);

bool eval_solution(const McDeval::Solution& sol,
                   std::vector<McDeval::Risk>& risks);

McDeval::Solution mutate(const McDeval::Solution& sol,
                         const std::function<double(void)>& rnd01,
                         double shrink_scale);

McDeval::Solution crossover(const McDeval::Solution& s1,
                            const McDeval::Solution& s2,
                            const std::function<double(void)>& rnd01);

std::vector<double> calculate_objectives(
    const EA::Genetic<McDeval::Solution,
                      std::vector<McDeval::Risk>>::thisChromosomeType& x);

void report_generation(
    int generation_number,
    const EA::GenerationType<McDeval::Solution, std::vector<McDeval::Risk>>&
        generation,
    const std::vector<unsigned>& pareto_front_indices);

class OpenGASingleton {
  public:
    const McDeval::Configuration* app_config_p = nullptr;
    std::vector<std::unique_ptr<McDeval::Evaluator>> evaluators;
    std::unique_ptr<Plotter2D> plotter = nullptr;
    unsigned log_frequency             = 1;
    unsigned plot_frequency            = 1;
    bool pause_after_plot              = false;

    static OpenGASingleton& get()
    {
        static OpenGASingleton open_ga_singleton;
        return open_ga_singleton;
    }
};
}

MultiObjectiveGeneticAlgorithm::MultiObjectiveGeneticAlgorithm(
    const AlgorithmConfiguration& alg_config,
    const McDeval::Configuration& app_config)
    : MultiObjectiveAlgorithm(alg_config, app_config)
{
    // Makes sure all parameters required by this algorithm are set in the
    // algorithm configuration object. Note parameters common for all multi
    // objective algorithms are checked in the super class constructor, so here,
    // only parameters specific to this algorithm are checked.
    validate_parameters();

    // Setting static objects so they can be accessed by static functions.
    auto&& ogas           = OpenGASingleton::get();
    ogas.app_config_p     = &app_config;
    ogas.log_frequency    = alg_config.log_frequency.value();
    ogas.plot_frequency   = alg_config.plot_frequency.value();
    ogas.pause_after_plot = alg_config.pause_after_plot.value();

    // Create evaluators and push them into static objects.
    ogas.evaluators.push_back(McDeval::Evaluator::create_evaluator(
        alg_config.evaluators[0].evaluator_type, app_config));
    ogas.evaluators.push_back(McDeval::Evaluator::create_evaluator(
        alg_config.evaluators[1].evaluator_type, app_config));

    ogas.plotter = std::make_unique<Plotter2D>(
        alg_config.evaluators[0].plot_range.value(),
        alg_config.evaluators[1].plot_range.value());

    // Algorithm object configuration.
    ga.problem_mode       = EA::GA_MODE::NSGA_III;
    ga.generation_max     = *alg_config.generation_max;
    ga.population         = *alg_config.population_size;
    ga.mutation_rate      = *alg_config.mutation_rate;
    ga.crossover_fraction = 1.0 - *alg_config.mutation_rate;

    // Register functions used by the algorithm.
    ga.init_genes              = init_genes;
    ga.calculate_MO_objectives = calculate_objectives;
    ga.init_genes              = init_genes;
    ga.eval_solution           = eval_solution;
    ga.mutate                  = mutate;
    ga.crossover               = crossover;
    ga.MO_report_generation    = report_generation;

    // Disable multi-threading since evaluation is relatively short.
    ga.multi_threading = false;
}

std::vector<McDeval::Solution> MultiObjectiveGeneticAlgorithm::run()
{
    ga.solve();

    pareto_solutions.clear(); // Clear pareto solutions of the previous run.

    // Fill pareto solutions vector with solutions from this run.
    std::vector<unsigned> pareto_indices = ga.last_generation.fronts[0];
    for (unsigned i : pareto_indices) {
        pareto_solutions.push_back(ga.last_generation.chromosomes[i].genes);
    }

    return pareto_solutions;
}

std::vector<std::vector<McDeval::Risk>>
MultiObjectiveGeneticAlgorithm::get_nondominated_solution_risks() const
{
    std::vector<std::vector<McDeval::Risk>> pareto_risks;
    pareto_risks.reserve(pareto_solutions.size());

    // Evaluate each pareto solution and save its risks.
    for (const McDeval::Solution& solution : pareto_solutions) {
        // Evaluate solution.
        std::vector<McDeval::Risk> solution_risks;
        eval_solution(solution, solution_risks);

        // Push risks into vector that holds risks for each pareto solution.
        pareto_risks.push_back(solution_risks);
    }

    return pareto_risks;
}

void MultiObjectiveGeneticAlgorithm::validate_parameters() const
{
    if (!alg_config.mutation_rate.has_value()) { // Mutation rate.
        throw_missing_parameter_error("mutation_rate");
    }

    if (!alg_config.population_size.has_value()) { // Population size.
        throw_missing_parameter_error("population_size");
    }
}

namespace {

void init_genes(McDeval::Solution& s,
                const std::function<double(void)>& /*rnd01*/)
{
    // Initialize solution with random mapping of fragments and components.
    auto&& ogas = OpenGASingleton::get();
    s           = Random::solution(*ogas.app_config_p);
}

bool eval_solution(const McDeval::Solution& sol,
                   std::vector<McDeval::Risk>& risks)
{
    auto&& ogas = OpenGASingleton::get();
    risks.clear(); // Make sure risks vector is empty. May not be necessary.
    for (auto&& evaluator : ogas.evaluators) {
        risks.push_back(evaluator->evaluate(sol));
    }
    return true;
}

McDeval::Solution mutate(const McDeval::Solution& s,
                         const std::function<double(void)>& /*rnd01*/,
                         double /*shrink_scale*/)
{
    auto&& ogas = OpenGASingleton::get();
    if (Random::boolean()) {
        // Create a random solution.
        return Random::solution(*ogas.app_config_p);
    }

    McDeval::Solution sol(s);
    // Random server where assignable object will be reassigned.
    const McDeval::Server& server = Random::server(*ogas.app_config_p);

    bool fragment = Random::boolean();
    if (fragment) { // Random fragment should be reassigned.
        const McDeval::Fragment& fragment =
            Random::fragment(*ogas.app_config_p);
        sol.set_server(fragment, server);
    } else { // Random component should be reassigned.
        const McDeval::Component& component =
            Random::component(*ogas.app_config_p);
        sol.set_server(component, server);
    }
    return sol;
}

McDeval::Solution crossover(const McDeval::Solution& s1,
                            const McDeval::Solution& s2,
                            const std::function<double(void)>& /*rnd01*/)
{
    auto&& ogas = OpenGASingleton::get();
    McDeval::Solution child;

    // Get each fragment server randomly from first or the second parent.
    for (auto&& f : ogas.app_config_p->fragments) {
        auto&& s = Random::boolean() ? s1.get_server(f) : s2.get_server(f);
        child.set_server(f, s);
    }

    // Get each component server randomly from first or the second parent.
    for (auto&& c : ogas.app_config_p->components) {
        auto&& s = Random::boolean() ? s1.get_server(c) : s2.get_server(c);
        child.set_server(c, s);
    }

    return child;
}

std::vector<double> calculate_objectives(
    const EA::Genetic<McDeval::Solution,
                      std::vector<McDeval::Risk>>::thisChromosomeType& x)
{
    // Convert risk objects into plain double values.
    std::vector<double> risks;
    risks.reserve(x.middle_costs.size());
    for (auto&& r : x.middle_costs) { risks.emplace_back(r); }
    return risks;
}

void report_generation(
    int generation_number,
    const EA::GenerationType<McDeval::Solution, std::vector<McDeval::Risk>>&
        generation,
    const std::vector<unsigned int>& pareto_front_indices)
{
    auto&& ogas = OpenGASingleton::get();
    if (generation_number % ogas.log_frequency == 0) {
        std::cout << "GENERATION: " << generation_number
                  << "\nPareto front size: " << pareto_front_indices.size()
                  << "\n\n";
    }

    if (generation_number % ogas.plot_frequency == 0) {
        // Get risks of last pareto generation and plot them.
        std::vector<std::vector<double>> risks;
        for (auto&& chromosome : generation.chromosomes) {
            risks.emplace_back(chromosome.objectives);
        }
        ogas.plotter->plot(risks);
        if (ogas.pause_after_plot) {
            Log::normal("Plot complete. Press ENTER to continue...");
            std::getchar();
        }
    }
}

}

}
