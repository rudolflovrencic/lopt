project('Lopt', 'cpp',
        version: '0.1.0',
        default_options: ['warning_level=3', 'cpp_std=c++17'])

sources = files(
    'src/Main.cpp',
    'src/Parser.cpp',
    'src/Random.cpp',
    'src/Gnuplot.cpp',
    'src/Configuration.cpp',
    'src/algorithm/Algorithm.cpp',
    'src/MultiObjectivePlotter.cpp',
    'src/algorithm/ecf/Mutations.cpp',
    'src/algorithm/ecf/Crossovers.cpp',
    'src/algorithm/ecf/Evaluations.cpp',
    'src/algorithm/ecf/LayoutGenotype.cpp',
    'src/algorithm/ecf/ConfigurationFile.cpp',
    'src/algorithm/ecf/SingleObjectiveGeneticAlgorithm.cpp',
    'src/algorithm/ppes/Mutations.cpp',
    'src/algorithm/ppes/Evaluations.cpp',
    'src/algorithm/ppes/PredatorPreyAlgorithm.cpp',
    'src/algorithm/openga/MultiObjectiveGeneticAlgorithm.cpp',
    'src/algorithm/openga/SingleObjectiveGeneticAlgorithm.cpp'
)

include_dir = include_directories('include', 'vendor/include')

thread_dep = dependency('threads')
fmt_dep    = dependency('fmt', version: '>=7.0.0',
                        fallback: ['fmt', 'fmt_dep'])

ecf_dep    = dependency('ECF', version: '>=1.4.2',
                        fallback: ['ECF', 'ecf_dep'])

mcdeval_dep = subproject('McDeval').get_variable('mcdeval_dep')
ppes_dep = subproject('PPES').get_variable('ppes_dep')

lopt = executable('Lopt', sources,
                  include_directories: include_dir,
                  dependencies: [thread_dep, fmt_dep, ecf_dep, mcdeval_dep,
                                 ppes_dep])
