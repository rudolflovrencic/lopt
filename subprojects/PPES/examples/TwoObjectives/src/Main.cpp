#include <PPES.hpp>

#include "Loader.hpp"
#include "TwoDimensionalObjective.hpp"
#include "RandomDoubleMutation.hpp"

#include <iostream>

typedef std::array<double, 2> SolutionType;

int main(int argc, char* argv[])
{
    if (argc != 2) {
        std::stringstream ss;
        ss << "Incorrect number of command line arguments.\n"
           << "Program usage:\n\t" << argv[0] << " CONFIGURATION_FILE.ini";
        PPES::Log::log_error(ss.str());
        return 1;
    }

    try {
        // Create objective (fitness) functions.
        std::vector<std::unique_ptr<PPES::Objective<SolutionType>>> objs;
        objs.emplace_back(std::make_unique<F1>());
        objs.emplace_back(std::make_unique<F2>());

        // Create mutation operators.
        std::vector<std::unique_ptr<PPES::Mutation<SolutionType>>> muts;
        muts.emplace_back(std::make_unique<RandomDoubleMutation>());

        // Create algorithm configuration object. Here, it is loaded from file.
        auto configuration = Loader(argv[1]).create_configuration();

        // Create algorithm object.
        PPES::Algorithm<SolutionType> a(configuration, objs, muts);

        // Run the algorithm and get nondominated solutions when finished.
        a.start();
        std::vector<SolutionType> ndom_sols = a.get_nondominated_solutions();

        // Report nondominated solutions and their fitness values.
        std::stringstream ss;
        ss << "Nondominated solutions:\n    SOLUTION          FITNESS\n";
        for (auto&& s : ndom_sols) {
            double f1 = objs[0]->evaluate(s);
            double f2 = objs[1]->evaluate(s);
            ss << std::setprecision(2) << std::fixed << "  {" << s[0] << ", "
               << s[1] << '}' << "  =>  [" << f1 << ", " << f2 << ']'
               << std::endl;
        }
        PPES::Log::log_note(ss.str());
    } catch (const std::runtime_error& error) {
        PPES::Log::log_error(error.what());
        return 1;
    }
    return 0;
}
