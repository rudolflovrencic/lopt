#include "TwoDimensionalObjective.hpp"

F1::F1() : Objective(true, "F1"), n_variables(2) {}

double F1::evaluate(const std::array<double, 2>& x) const
{
    return x[0] * x[0];
}

std::unique_ptr<PPES::Objective<std::array<double, 2>>> F1::clone() const
{
    return std::make_unique<F1>(*this);
}

F2::F2() : Objective(true, "F2"), n_variables(2) {}

double F2::evaluate(const std::array<double, 2>& x) const
{
    return (1.0 + x[1] * x[1]) / (x[0] * x[0]);
}

std::unique_ptr<PPES::Objective<std::array<double, 2>>> F2::clone() const
{
    return std::make_unique<F2>(*this);
}
