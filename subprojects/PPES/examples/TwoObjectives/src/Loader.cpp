#include "Loader.hpp"

#include <iterator>

#include "PPES.hpp"

namespace {
void no_valid_ini_value(const std::string& filename,
                        const std::string& section,
                        const std::string& attribute,
                        const std::string& default_value);
}

Loader::Loader(const std::string& filename) : r(filename), filename(filename)
{
    if (r.ParseError() != 0) {
        throw std::runtime_error("Could not parse configuration file: " +
                                 filename);
    }
}

size_t Loader::world_width() const
{
    const char* sec_name = sec_world;
    const char* att_name = att_world_width;
    auto default_value   = def_world_width;

    long w = r.GetInteger(sec_name, att_name, 0);
    if (w <= 1l) { // No valid value provided in the ini file.
        std::stringstream iss;
        iss << default_value;
        no_valid_ini_value(filename, sec_name, att_name, iss.str());
        w = default_value;
    }
    return w;
}

size_t Loader::world_height() const
{
    const char* sec_name = sec_world;
    const char* att_name = att_world_height;
    auto default_value   = def_world_height;

    long h = r.GetInteger(sec_name, att_name, 0);
    if (h <= 1l) { // No valid value provided in the ini file.
        std::stringstream iss;
        iss << default_value;
        no_valid_ini_value(filename, sec_name, att_name, iss.str());
        h = default_value;
    }
    return h;
}

size_t Loader::n_predators_per_objective() const
{
    const char* sec_name = sec_predator;
    const char* att_name = att_n_predators_per_objective;
    auto default_value   = def_n_predators_per_objective;

    long n_predators_po = r.GetInteger(sec_name, att_name, 0);
    if (n_predators_po < 1l) { // No valid value provided in the ini file.
        std::stringstream iss;
        iss << default_value;
        no_valid_ini_value(filename, sec_name, att_name, iss.str());
        n_predators_po = default_value;
    }
    return n_predators_po;
}

size_t Loader::max_iterations() const
{
    const char* sec_name = sec_algorithm;
    const char* att_name = att_max_iterations;
    auto default_value   = def_max_iterations;

    long max_iter = r.GetInteger(sec_name, att_name, 0);
    if (max_iter <= 1l) { // No valid value provided in the ini file.
        std::stringstream iss;
        iss << default_value;
        no_valid_ini_value(filename, sec_name, att_name, iss.str());
        max_iter = default_value;
    }
    return max_iter;
}

size_t Loader::log_frequency() const
{
    const char* sec_name = sec_logging;
    const char* att_name = att_log_frequency;

    long log_freq = r.GetInteger(sec_name, att_name, 0);
    if (log_freq < 1l) { // No valid value provided in the ini file.
        PPES::Log::log_note("Logging disabled.");
        log_freq = 0;
    }
    return log_freq;
}

bool Loader::pause_after_plot() const
{
    const char* sec_name = sec_plotting;
    const char* att_name = att_pause_after_plot;

    bool log_freq = r.GetBoolean(sec_name, att_name, false);
    return log_freq;
}

size_t Loader::plot_frequency() const
{
    const char* sec_name = sec_plotting;
    const char* att_name = att_plot_frequency;

    long plot_freq = r.GetInteger(sec_name, att_name, 0);
    if (plot_freq < 1l) { // No valid value provided in the ini file.
        PPES::Log::log_note("Plotting disabled.");
        plot_freq = 0;
    }
    return plot_freq;
}

unsigned Loader::log_precision() const
{
    const char* sec_name = sec_logging;
    const char* att_name = att_log_precision;
    auto default_value   = def_log_precision;

    long log_precision = r.GetInteger(sec_name, att_name, 0);
    if (log_precision < 1l) { // No valid value provided in the ini file.
        std::stringstream iss;
        iss << default_value;
        no_valid_ini_value(filename, sec_name, att_name, iss.str());
        log_precision = default_value;
    }
    return log_precision;
}

std::string Loader::limits() const
{
    const char* sec_name = sec_prey;
    const char* att_name = att_limits;
    auto default_value   = def_limits;

    std::string limits_str = r.Get(sec_name, att_name, "");
    if (limits_str.length() == 0) { // No valid value provided in the ini file.
        std::stringstream iss;
        iss << default_value;
        no_valid_ini_value(filename, sec_name, att_name, iss.str());
        limits_str = default_value;
    }
    return limits_str;
}

std::string Loader::plot_range() const
{
    const char* sec_name = sec_plotting;
    const char* att_name = att_plot_range;
    auto default_value   = def_plot_range;

    std::string limits_str = r.Get(sec_name, att_name, "");
    if (limits_str.length() == 0) { // No valid value provided in the ini file.
        std::stringstream iss;
        iss << default_value;
        no_valid_ini_value(filename, sec_name, att_name, iss.str());
        limits_str = default_value;
    }
    return limits_str;
}

std::string Loader::objectives() const
{
    const char* sec_name = sec_predator;
    const char* att_name = att_objectives;
    auto default_value   = def_objectives;

    std::string obj_str = r.Get(sec_name, att_name, "");
    if (obj_str.length() == 0) { // No valid value provided in the ini file.
        std::stringstream iss;
        iss << default_value;
        no_valid_ini_value(filename, sec_name, att_name, iss.str());
        obj_str = default_value;
    }
    return obj_str;
}

std::string Loader::mutations() const
{
    const char* sec_name = sec_algorithm;
    const char* att_name = att_mutations;
    auto default_value   = def_mutations;

    std::string mut_str = r.Get(sec_name, att_name, "");
    if (mut_str.length() == 0) { // No valid value provided in the ini file.
        std::stringstream iss;
        iss << default_value;
        no_valid_ini_value(filename, sec_name, att_name, iss.str());
        mut_str = default_value;
    }
    return mut_str;
}

PPES::Configuration Loader::create_configuration()
{
    PPES::Configuration c(world_width(),
                          world_height(),
                          n_predators_per_objective(),
                          max_iterations(),
                          log_frequency(),
                          plot_frequency(),
                          log_precision(),
                          pause_after_plot(),
                          parse_plot_range(plot_range()));

    if (c.log_frequency == 0) { // Log frequency not provided in the file.
        c.log_frequency = c.max_iterations; // Disable logging.
    }
    if (c.plot_frequency == 0) { // Plot frequency not provided in the file.
        c.plot_frequency = c.max_iterations; // Disable plot.
    }

    return c;
}

/** Create vector of plot range limits based on the string data provided.
 * @param o_str Space separated plot range limits. */
std::vector<std::pair<double, double>>
Loader::parse_plot_range(const std::string& range_str)
{
    std::vector<std::pair<double, double>> plot_range;

    std::istringstream iss(range_str);
    std::vector<std::string> tokens{std::istream_iterator<std::string>{iss},
                                    std::istream_iterator<std::string>{}};
    for (size_t i = 0, n = tokens.size() / 2; i < n; i++) {
        double lower = std::atof(tokens[2 * i].c_str());
        double upper = std::atof(tokens[2 * i + 1].c_str());

        if (lower > upper) {
            std::stringstream ss;
            ss << "Invalid plot range limits provided. Lower plot range limit ("
               << lower << ") is larger than upper plot range limit (" << upper
               << ") so proper vector of plot range limits cannot be created.";
            throw std::runtime_error(ss.str());
        }

        plot_range.emplace_back(lower, upper);
    }

    return plot_range;
}

namespace {
void no_valid_ini_value(const std::string& filename,
                        const std::string& section,
                        const std::string& attribute,
                        const std::string& default_value)
{
    std::stringstream ss;
    ss << "No valid value provided for attribute \"" << attribute
       << "\" in section \"" << section << "\" inside file \"" << filename
       << "\".\nSetting to default value: " << default_value;
    PPES::Log::log_warning(ss.str());
}

}
