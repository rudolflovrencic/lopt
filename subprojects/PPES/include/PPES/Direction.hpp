#ifndef PPES_DIRECTION_HPP
#define PPES_DIRECTION_HPP

#include <ostream>

namespace PPES {

enum class Direction : int {
    N = 0, NW = 1, W = 2, SW = 3, S = 4, SE = 5, E = 6, NE = 7
};

inline std::ostream& operator<<(std::ostream& s, Direction d)
{
    switch (d) {
    case Direction::N:
        return s << 'N';
    case Direction::NW:
        return s << "NW";
    case Direction::W:
        return s << 'W';
    case Direction::SW:
        return s << "SW";
    case Direction::S:
        return s << 'S';
    case Direction::SE:
        return s << "SE";
    case Direction::E:
        return s << 'E';
    case Direction::NE:
        return s << "NE";
    default:
        throw std::runtime_error(
                "Operator \"<<\" not handled for provided direction.");
    }
}

}

#endif
