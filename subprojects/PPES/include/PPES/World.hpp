#ifndef PPES_WORLD_HPP
#define PPES_WORLD_HPP

#include "Predator.hpp"

namespace PPES {

template<typename SolutionT>
class World {
  public:
    /** World height (number of rows). */
    const size_t height;

    /** World width. (number of columns) */
    const size_t width;

    /** Contains preys and defines their location. */
    std::vector<Prey<SolutionT>> preys;

    /** Contains predators. */
    std::vector<Predator<SolutionT>> predators;

  private:
    /** Objectives assigned to predators. Used for calculating nondominated
     * solutions (the pareto frontier). */
    std::vector<std::unique_ptr<Objective<SolutionT>>> objs;

  public:
    World(size_t height,
          size_t width,
          size_t n_predators_per_objective,
          const std::vector<std::unique_ptr<Objective<SolutionT>>>& objs);

    HaloArray<const Prey<SolutionT>*> get_surrounding_preys(Coordinate c) const;

    Coordinate get_coordinate(Coordinate c, Direction d) const;

    Prey<SolutionT>& get_prey(Coordinate c);

    std::vector<SolutionT> get_nondominated_solutions() const;

  private:
    const Prey<SolutionT>& get_prey(Coordinate c) const;

    HaloArray<Coordinate> get_surrounding_coordinates(Coordinate c) const;

    bool a_dominates_b(const SolutionT& a, const SolutionT& b) const;
};

template<typename SolutionT>
World<SolutionT>::World(
    size_t height,
    size_t width,
    size_t n_predators_per_objective,
    const std::vector<std::unique_ptr<Objective<SolutionT>>>& objs)
    : height(height), width(width)
{
    // Reserve space for all preys and create them afterwards.
    preys.reserve(width * height);
    for (size_t i = 0, n = width * height; i < n; i++) { preys.emplace_back(); }

    // Reserve space for all predators and create them afterwards.
    predators.reserve(n_predators_per_objective * objs.size());
    for (const std::unique_ptr<Objective<SolutionT>>& o : objs) {
        for (size_t i = 0; i < n_predators_per_objective; i++) {
            // Clone the objective function.
            std::unique_ptr<Objective<SolutionT>> obj = o->clone();

            // Create predator at random location (owner of cloned objective).
            predators.emplace_back(std::move(obj), width, height);
        }
    }

    // Create a copy of each objective and store it in worlds internal vector.
    // Those objectives are used for calculation of nondominated solutions.
    this->objs.reserve(objs.size());
    for (const std::unique_ptr<Objective<SolutionT>>& o : objs) {
        this->objs.emplace_back(o->clone());
    }
}

template<typename SolutionT>
HaloArray<const Prey<SolutionT>*>
World<SolutionT>::get_surrounding_preys(Coordinate c) const
{
    HaloArray<Coordinate> cs = get_surrounding_coordinates(c);
    HaloArray<const Prey<SolutionT>*> ps;
    for (size_t i = 0, n = cs.size(); i < n; i++) { ps[i] = &get_prey(cs[i]); }
    return ps;
}

template<typename SolutionT>
Coordinate World<SolutionT>::get_coordinate(Coordinate c, Direction d) const
{
    size_t up    = c.row != 0 ? c.row - 1 : height - 1;
    size_t down  = c.row != height - 1 ? c.row + 1 : 0;
    size_t left  = c.column != 0 ? c.column - 1 : width - 1;
    size_t right = c.column != width - 1 ? c.column + 1 : 0;

    switch (d) {
        case Direction::N: return {up, c.column};   // North
        case Direction::NW: return {up, left};      // North-West
        case Direction::W: return {c.row, left};    // West
        case Direction::SW: return {down, left};    // South-West
        case Direction::S: return {down, c.column}; // South
        case Direction::SE: return {down, right};   // South-East
        case Direction::E: return {c.row, right};   // East
        case Direction::NE: return {up, right};     // North-East
        default: throw std::runtime_error("Unknown direction was provided.");
    }
}

template<typename SolutionT>
Prey<SolutionT>& World<SolutionT>::get_prey(Coordinate c)
{
    return preys.at(c.row * width + c.column);
}

template<typename SolutionT>
std::vector<SolutionT> World<SolutionT>::get_nondominated_solutions() const
{
    std::vector<SolutionT> nondominated_solutions;
    nondominated_solutions.reserve(preys.size() / 3); // Reserve some space.

    for (const Prey<SolutionT>& px : preys) {
        const SolutionT& x = px.get_data();       // Shortcut reference.
        bool dominated     = false;               // Assume 'x' is nondominated.
        for (const Prey<SolutionT>& py : preys) { // Find dominating solution.
            const SolutionT& y = py.get_data();   // Shortcut reference for y.
            if (a_dominates_b(y, x)) {            // Check if 'y' dominates 'x'.
                dominated = true;                 // 'x' is dominated.
                break;
            }
        }
        // Save solution if it is not dominated (copy construction).
        if (!dominated) { nondominated_solutions.emplace_back(x); }
    }

    return nondominated_solutions;
}

template<typename SolutionT>
const Prey<SolutionT>& World<SolutionT>::get_prey(Coordinate c) const
{
    return preys.at(c.row * width + c.column);
}

template<typename SolutionT>
HaloArray<Coordinate>
World<SolutionT>::get_surrounding_coordinates(Coordinate c) const
{
    size_t up    = c.row != 0 ? c.row - 1 : height - 1;
    size_t down  = c.row != height - 1 ? c.row + 1 : 0;
    size_t left  = c.column != 0 ? c.column - 1 : width - 1;
    size_t right = c.column != width - 1 ? c.column + 1 : 0;

    return {Coordinate(up, c.column),   // North
            Coordinate(up, left),       // North-West
            Coordinate(c.row, left),    // West
            Coordinate(down, left),     // South-West
            Coordinate(down, c.column), // South
            Coordinate(down, right),    // South-East
            Coordinate(c.row, right),   // East
            Coordinate(up, right)};     // North-East
}

template<typename SolutionT>
bool World<SolutionT>::a_dominates_b(const SolutionT& a,
                                     const SolutionT& b) const
{
    bool one_better = false;
    for (const std::unique_ptr<Objective<SolutionT>>& o : objs) {
        double f_a = o->evaluate(a);
        double f_b = o->evaluate(b);

        if (o->minimization) { // Objective is to minimize.
            if (f_a < f_b) {   // Strictly better component.
                one_better = true;
            } else if (f_a > f_b) { // Strictly worse component.
                return false;
            }
        } else {             // Objective is to maximize.
            if (f_a > f_b) { // Strictly better component.
                one_better = true;
            } else if (f_a < f_b) { // Strictly worse component.
                return false;
            }
        }
    }
    return one_better;
}

}

#endif
