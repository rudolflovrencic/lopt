#ifndef PPES_COORDINATE_HPP
#define PPES_COORDINATE_HPP

#include <ostream>

namespace PPES {

struct Coordinate {
    size_t row;
    size_t column;

    Coordinate(size_t row, size_t column) : row(row), column(column) {}

    friend std::ostream& operator<<(std::ostream& s, const Coordinate& c)
    {
        return s << '(' << c.row << ',' << c.column << ')';
    }
};

}

#endif
