#ifndef PPES_LOG_HPP
#define PPES_LOG_HPP

#include <fmt/color.h>
#include <fmt/core.h>
#include <fmt/ostream.h>

#include "Termination.hpp"
#include "Objective.hpp"

#include <sstream>
#include <iomanip>

namespace PPES {

class Log {
  public:
#ifndef NLOG
    template<typename... T>
    static void normal(std::string_view format_string, const T&... args)
    {
        fmt::print(format_string, args...);
        fmt::print("\n");
    }

    template<typename... T>
    static void success(std::string_view format_string, const T&... args)
    {
        fmt::print(fg(fmt::color::green), format_string, args...);
        fmt::print("\n");
    }

    template<typename... T>
    static void warn(std::string_view format_string, const T&... args)
    {
        fmt::print(fg(fmt::color::yellow), format_string, args...);
        fmt::print("\n");
    }

    template<typename... T>
    static void error(std::string_view format_string, const T&... args)
    {
        fmt::print(stderr, fg(fmt::color::red), format_string, args...);
        fmt::print(stderr, "\n");
    }
#else
    template<typename... T>
    static void normal(std::string_view /*format_string*/, const T&... /*args*/)
    {}

    template<typename... T>
    static void success(std::string_view /*format_string*/,
                        const T&... /*args*/)
    {}

    template<typename... T>
    static void warn(std::string_view /*format_string*/, const T&... /*args*/)
    {}

    template<typename... T>
    static void error(std::string_view /*format_string*/, const T&... /*args*/)
    {}
#endif
};

template<typename SolutionT>
class Algorithm;

template<typename SolutionT>
class Logger {
  private:
    const Configuration& c;
    const std::vector<std::unique_ptr<Objective<SolutionT>>>& objs;

  public:
    Logger(const Configuration& c,
           const std::vector<std::unique_ptr<Objective<SolutionT>>>& objs);

    void log_algorithm_state(const Algorithm<SolutionT>& a) const;
    void log_termination_met(const TerminationCriteria<SolutionT>& tc) const;
};

template<typename SolutionT>
Logger<SolutionT>::Logger(
    const Configuration& c,
    const std::vector<std::unique_ptr<Objective<SolutionT>>>& objs)
    : c(c), objs(objs)
{}

template<typename SolutionT>
void Logger<SolutionT>::log_algorithm_state(const Algorithm<SolutionT>& a) const
{
    // Initialize only once.
    static std::vector<double> best(objs.size()); // Best by each criteria.

    // Set each variable to its worst value.
    for (size_t i = 0, n = objs.size(); i < n; i++) {
        best[i] = objs[i]->minimization ? std::numeric_limits<double>::max() :
                                          std::numeric_limits<double>::min();
    }

    // Evaluate each prey for at each objective and remember.
    for (const auto& p : a.world.preys) {
        for (size_t i = 0, n = objs.size(); i < n; i++) {
            const Objective<SolutionT>& o = *objs[i];
            double v                      = o.evaluate(p.get_data());
            if (o.minimization && v < best[i]) { best[i] = v; }
            if (!o.minimization && v > best[i]) { best[i] = v; }
        }
    }

    // Set precision of the string stream and log current iteration.
    std::stringstream preamble_ss;
    int n_digits = log10(static_cast<double>(c.max_iterations)) + 1u;
    preamble_ss << std::setw(n_digits) << a.iteration; // Iteration to string.

    std::stringstream message_ss;
    if (a.iteration != 0) {
        message_ss << "Seconds elapsed: " << a.timer.total() << '\n';
    }
    message_ss << std::fixed << std::setprecision(c.log_precision);
    for (size_t i = 0, n = best.size(); i < n; i++) {
        message_ss << *objs[i] << ": " << best[i] << "  ";
    }
    Log::normal("[{}] {}", preamble_ss.str(), message_ss.str());
}

template<typename SolutionT>
void Logger<SolutionT>::log_termination_met(
    const TerminationCriteria<SolutionT>& tc) const
{
    Log::success("{}", tc);
}

}

#endif
