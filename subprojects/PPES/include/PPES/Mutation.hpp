#ifndef PPES_MUTATION_HPP
#define PPES_MUTATION_HPP

#include <vector>
#include <string>
#include <memory>

namespace PPES {

template<typename SolutionT>
class Mutation {
  public:
    const std::string name;

  public:
    explicit Mutation(std::string name);
    virtual ~Mutation() = default;

    virtual void mutate(const SolutionT& src, SolutionT& dest) const = 0;

    template<typename T>
    friend std::ostream& operator<<(std::ostream& s, const Mutation<T>& m);
};

template<typename SolutionT>
Mutation<SolutionT>::Mutation(std::string name) : name(std::move(name))
{}

template<typename T>
std::ostream& operator<<(std::ostream& s, const Mutation<T>& m)
{
    return s << m.name;
}

}

#endif
