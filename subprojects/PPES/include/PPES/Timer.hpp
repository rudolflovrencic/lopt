#ifndef PPES_TIMER_HPP
#define PPES_TIMER_HPP

#include <chrono>

namespace PPES {

class Timer {
private:
    std::chrono::time_point<std::chrono::system_clock> start_time;
    std::chrono::time_point<std::chrono::system_clock> lap_start_time;
public:
    /** Starts the time measurement. */
    void start()
    {
        start_time = lap_start_time = std::chrono::system_clock::now();
    }

    /** Returns running time in seconds since last time this function was
     * called or since timer start if this function was not yet called.
     * Does not stop the timer.
     * @return Running time in seconds since lap start. */
    double lap()
    {
        auto now = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = now - lap_start_time;
        lap_start_time = now;
        return diff.count();
    }

    /** Returns total running time in seconds without stopping the timer.
     * @return Running time in seconds. */
    double total() const
    {
        auto now = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = now - start_time;
        return diff.count();
    }

    /** Stop the time and returns total running time in seconds.
     * @return Total running time in seconds. */
    double stop()
    {
        auto now = std::chrono::system_clock::now();
        std::chrono::duration<double> diff = now - start_time;
        return diff.count();
    }
};

}

#endif
