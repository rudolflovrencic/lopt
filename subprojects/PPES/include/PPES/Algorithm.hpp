#ifndef PPES_ALGORITHM_HPP
#define PPES_ALGORITHM_HPP

#include "Timer.hpp"
#include "World.hpp"
#include "Configuration.hpp"
#include "Log.hpp"
#include "Termination.hpp"
#include "Mutation.hpp"
#include "Plotter.hpp"

namespace PPES {

template<typename SolutionT>
class Algorithm {
  private:
    Timer timer;
    World<SolutionT> world;
    Plotter<SolutionT> plotter;
    Logger<SolutionT> logger;
    unsigned long long iteration;
    unsigned long long log_frequency;
    unsigned long long plot_frequency;
    std::vector<std::unique_ptr<TerminationCriteria<SolutionT>>> tcs;
    std::vector<std::unique_ptr<Mutation<SolutionT>>> mutations;

  public:
    /** Constructs an algorithm based on the given configuration object,
     * objectives (fitness functions) and mutation operators. Note that mutation
     * vector is not marked const as its members are moved into algorithms
     * internal vector. */
    Algorithm(Configuration& c,
              const std::vector<std::unique_ptr<Objective<SolutionT>>>& objs,
              std::vector<std::unique_ptr<Mutation<SolutionT>>>& muts);

    void start();
    unsigned long long get_current_iteration() const;

    /** Returns pareto front (nondominated solutions) of the last algorithm run.
     * Make sure that the algorithm has been run before calling this function.
     */
    std::vector<SolutionT> get_nondominated_solutions() const;

  private:
    bool termination_met() const;

    friend void
    Logger<SolutionT>::log_algorithm_state(const Algorithm<SolutionT>& a) const;
    friend void Plotter<SolutionT>::plot(const Algorithm<SolutionT>& a);
};

template<typename SolutionT>
Algorithm<SolutionT>::Algorithm(
    Configuration& c,
    const std::vector<std::unique_ptr<Objective<SolutionT>>>& objs,
    std::vector<std::unique_ptr<Mutation<SolutionT>>>& muts)
    : world(c.world_width, c.world_height, c.n_predators_per_objective, objs),
      plotter(c, objs), // Plotter has to be aware of optimization objectives.
      logger(c, objs),  // Logger has to be aware of optimization objectives.
      iteration(0ull),
      log_frequency(c.log_frequency),
      plot_frequency(c.plot_frequency)
{
    tcs = TerminationCriteria<SolutionT>::get_termination_criteria(c, *this);
    for (std::unique_ptr<Mutation<SolutionT>>& mut : muts) {
        mutations.emplace_back(std::move(mut));
    }
}

template<typename SolutionT>
void Algorithm<SolutionT>::start()
{
    static constexpr std::size_t surrounding_cells = 8;

    timer.start(); // Start measuring time.

    for (iteration = 0ull; !termination_met(); iteration++) {
        if (!(iteration % log_frequency)) { logger.log_algorithm_state(*this); }
        if (!(iteration % plot_frequency)) { plotter.plot(*this); }
        for (Predator<SolutionT>& predator : world.predators) {
            // Get preys around the predator.
            std::array<const Prey<SolutionT>*, surrounding_cells> psp =
                world.get_surrounding_preys(predator.coordinate);

            // Identify the direction of the worst prey.
            Direction d_worst = predator.evaluate_prey(psp);

            // Get coordinate of the worst prey.
            Coordinate wpc = world.get_coordinate(predator.coordinate, d_worst);

            // Get preys surrounding the worst prey.
            std::array<const Prey<SolutionT>*, surrounding_cells> wpsp =
                world.get_surrounding_preys(wpc);

            // Get random prey surrounding the worst prey to mutate.
            const Prey<SolutionT>& ptm =
                *wpsp[Random::get_random_int(0, surrounding_cells - 1)];

            // Prey to replace.
            Prey<SolutionT>& ptr = world.get_prey(wpc);

            // Pick a random mutation and apply it to best prey (replace worst).
            ptr.replace_with_mutated(
                ptm,
                *mutations[Random::get_random_int(0, mutations.size() - 1)]);

            // Predator takes a walk in the random direction.
            predator.walk(world);
        }
    }
    timer.stop();        // Stop measuring time.
    plotter.plot(*this); // Plot when the algorithm is finished.
}

template<typename SolutionT>
unsigned long long Algorithm<SolutionT>::get_current_iteration() const
{
    return iteration;
}

template<typename SolutionT>
bool Algorithm<SolutionT>::termination_met() const
{
    for (const std::unique_ptr<TerminationCriteria<SolutionT>>& tc : tcs) {
        if (tc->is_met()) {
            logger.log_termination_met(*tc);
            return true;
        }
    }
    return false;
}

template<typename SolutionT>
std::vector<SolutionT> Algorithm<SolutionT>::get_nondominated_solutions() const
{
    return world.get_nondominated_solutions();
}

}

#endif
