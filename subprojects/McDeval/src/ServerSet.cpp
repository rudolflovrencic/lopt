#include "McDeval/ServerSet.hpp"

#include <sstream>

namespace McDeval {

bool ServerPointerCompare::operator()(const Server* a, const Server* b) const
{
    return *a < *b;
}

ServerSet::ServerSet(const std::set<const Server*>& servers)
{
    for (const Server* s : servers) { this->servers.insert(s); }
}

void ServerSet::inplace_intersect(const ServerSet& other)
{
    std::set<const Server*, ServerPointerCompare> intersection;
    std::set_intersection(servers.begin(),
                          servers.end(),
                          other.servers.begin(),
                          other.servers.end(),
                          std::inserter(intersection, intersection.begin()));
    servers = intersection;
}

const Server& ServerSet::get_most_insecure_server() const
{
    if (empty()) {
        throw std::logic_error("Cannot get most insecure server of empty "
                               "server set.");
    }
    const Server* most_insecure = *servers.begin();
    for (const Server* s : servers) {
        if (s->get_trust() < most_insecure->get_trust()) { most_insecure = s; }
    }
    return *most_insecure;
}

bool ServerSet::empty() const
{
    return servers.empty();
}

ServerSetConstIterator ServerSet::begin() const
{
    return servers.cbegin();
}

ServerSetConstIterator ServerSet::end() const
{
    return servers.cend();
}

bool ServerSet::operator==(const ServerSet& other) const
{
    return servers == other.servers;
}

std::ostream& operator<<(std::ostream& s, const ServerSet& sr)
{
    if (sr.empty()) { return s << "{}"; }

    std::stringstream ss;
    ss << '{';
    for (auto&& server_ponter : sr.servers) { ss << *server_ponter << " "; }
    ss.seekp(-1, std::stringstream::cur);
    ss << "}";

    return s << ss.str();
}

}
