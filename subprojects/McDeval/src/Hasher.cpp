#include "McDeval/Hasher.hpp"

#include "McDeval/Resource.hpp"
#include "McDeval/Constraint.hpp"
#include "McDeval/Component.hpp"
#include "McDeval/Server.hpp"
#include "McDeval/Solution.hpp"

namespace {
std::hash<std::string> string_hasher;
}

std::size_t
std::hash<McDeval::Constraint>::operator()(const McDeval::Constraint& c) const
{
    return string_hasher(c.id);
}

std::size_t
std::hash<McDeval::Component>::operator()(const McDeval::Component& c) const
{
    return string_hasher(c.id);
}

std::size_t
std::hash<McDeval::Server>::operator()(const McDeval::Server& s) const
{
    return string_hasher(s.id);
}

std::size_t
std::hash<McDeval::Resource>::operator()(const McDeval::Resource& r) const
{
    return string_hasher(r.id);
}

std::size_t
std::hash<McDeval::Solution>::operator()(const McDeval::Solution& s) const
{
    size_t ret = 0;
    std::hash<McDeval::Server> sh;    // Server hash function.
    std::hash<McDeval::Component> ch; // Component hash function.
    for (auto&& csp : s.component_mapping) {
        ret ^= ch(*csp.first) + sh(*csp.second);
    }
    std::hash<McDeval::Resource> rh; // Resource hash function.
    for (auto&& fsp : s.fragment_mapping) {
        ret ^= rh(*fsp.first) + sh(*fsp.second);
    }
    return ret;
}
