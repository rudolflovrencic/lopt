#include "McDeval/Fragment.hpp"

#include "McDeval/Solution.hpp"

namespace McDeval {

Fragment::Fragment(const std::string& id, unsigned importance)
    : Resource(id, importance)
{}

void Fragment::add_destination(const Component& component)
{
    destinations.push_back(&component);
}

bool Fragment::destination_set() const
{
    return !destinations.empty();
}

ServerSet Fragment::server_reach(const Solution& s) const
{
    // Reach of a fragment is a server where that fragment is located and
    // servers of all components that access that fragment.
    std::set<const Server*> fragment_reach;

    // Add this fragments server.
    const Server& fs = s.get_server<Fragment>(*this); // This fragment server.
    fragment_reach.insert(&fs);

    // Add all destinations of this fragment.
    for (const Component* c : destinations) {
        const Server& cs = s.get_server<Component>(*c); // Destination server.
        fragment_reach.insert(&cs);
    }

    return ServerSet(fragment_reach);
}

}
