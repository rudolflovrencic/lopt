#include "McDeval/Result.hpp"

#include <stdexcept>

#include "McDeval/Component.hpp"
#include "McDeval/Solution.hpp"

namespace McDeval {

Result::Result(const std::string& id, unsigned importance)
    : Resource(id, importance), source(nullptr), destination(nullptr)
{}

void Result::set_source(const Component& source)
{
    if (source_set()) {
        throw std::logic_error("Result source cannot be set multiple times.");
    }
    this->source = &source;
}

const Component& Result::get_source() const
{
    if (!source_set()) {
        throw std::logic_error("Result source accessed, but not set.");
    }
    return *source;
}

void Result::set_destination(const Component& destination)
{
    if (destination_set()) {
        throw std::logic_error("Result destination cannot be set multiple "
                               "times.");
    }
    this->destination = &destination;
}

const Component& Result::get_destination() const
{
    if (!destination_set()) {
        throw std::logic_error("Result destination accessed, but not set.");
    }
    return *destination;
}

ServerSet Result::server_reach(const Solution& s) const
{
    // Reach of a result is only source and destination components.
    return ServerSet({&s.get_server<Component>(*source),
                      &s.get_server<Component>(*destination)});
}

bool Result::source_set() const
{
    return source != nullptr;
}

bool Result::destination_set() const
{
    return destination != nullptr;
}

}
