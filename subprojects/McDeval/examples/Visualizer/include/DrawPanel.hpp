#ifndef DRAW_PANEL_HPP
#define DRAW_PANEL_HPP

#include <wx/wx.h>

/** Abstract base class panels that can be drawn to. */
class DrawPanel : public wxPanel {
  public:
    DrawPanel(wxWindow* parent);

  private:
    /** Performs all drawing. */
    virtual void render(wxDC& dc) = 0;

    /** Called by the framework when the panel needs to be redrawn. */
    void on_paint_event(wxPaintEvent& event);

    DECLARE_EVENT_TABLE()
};

#endif
