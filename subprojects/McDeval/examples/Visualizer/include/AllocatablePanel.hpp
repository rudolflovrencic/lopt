#ifndef ALLOCATABLEPANEL_HPP
#define ALLOCATABLEPANEL_HPP

#include "DrawPanel.hpp"

class AllocatablePanel : public DrawPanel {
  public:
    const char type;  ///< Fragment ('F') or component ('C').
    const char index; ///< Fragment or component index.

  public:
    AllocatablePanel(wxWindow* parent, char type, char index);

  private:
    void on_mouse_down(wxMouseEvent& event);
    void on_mouse_enter(wxMouseEvent& event);
    void on_mouse_leave(wxMouseEvent& event);

    DECLARE_EVENT_TABLE()
};

class FragmentPanel : public AllocatablePanel {
  public:
    FragmentPanel(wxWindow* parent, char index);

  private:
    void render(wxDC& dc) override;
};

class ComponentPanel : public AllocatablePanel {
  public:
    ComponentPanel(wxWindow* parent, char index);

  private:
    void render(wxDC& dc) override;
};

#endif
