#ifndef APPLICATION_HPP
#define APPLICATION_HPP

#include "MainFrame.hpp"

#include <memory>

class Application : public wxApp {
  private:
    MainFrame* main_frame = nullptr;

  public:
    bool OnInit() override;
    bool OnExceptionInMainLoop() override;
    void OnUnhandledException() override;
};

// Declare entry point. Enables static fetch of application instance.
wxDECLARE_APP(Application);

#endif
