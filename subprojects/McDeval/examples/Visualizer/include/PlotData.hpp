#ifndef PLOT_DATA_HPP
#define PLOT_DATA_HPP

#include <mgl2/mgl.h>

#include <array>

class PlotData {
  private:
    std::vector<double> x_points;
    std::vector<double> y_points;

  public:
    PlotData(std::size_t reserve_size = 10);

    void add_point(std::array<double, 2> point);

    std::size_t size() const;
    bool empty() const;
    void clear();

    const std::vector<double>& get_x_points() const;
    const std::vector<double>& get_y_points() const;

    friend std::ostream& operator<<(std::ostream& s, const PlotData& pd);
};

#endif
