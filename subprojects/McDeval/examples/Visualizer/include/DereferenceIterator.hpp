#ifndef REFERENCEITERATOR_HPP
#define REFERENCEITERATOR_HPP

#include <cstddef>
#include <type_traits>

/** Reference iterator over a collection that stores pointers. This means that
  iteration is performed over references to objects. */
template<class BaseIterator>
class ReferenceIterator : public BaseIterator {
  public:
    using value_type =
        typename std::remove_pointer<typename BaseIterator::value_type>::type;
    using reference = value_type&;

    explicit ReferenceIterator(const BaseIterator& other) : BaseIterator(other)
    {}

    reference operator*() const { return *(this->BaseIterator::operator*()); }

    reference operator[](std::size_t n) const
    {
        return *(this->BaseIterator::operator[](n));
    }
};

#endif
