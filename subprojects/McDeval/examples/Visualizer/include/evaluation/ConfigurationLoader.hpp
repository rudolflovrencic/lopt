#ifndef CONFIGURATION_LOADER_HPP
#define CONFIGURATION_LOADER_HPP

#include "McDeval.hpp"

#include <filesystem>

namespace Evaluation::ConfigurationLoader {

/** Creates application configuration object based on configuration file. */
McDeval::Configuration load(const std::filesystem::path& filename);

}

#endif
