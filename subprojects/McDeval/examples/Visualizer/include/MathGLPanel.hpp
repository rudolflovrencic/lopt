#ifndef MATHGL_PANEL_HPP
#define MATHGL_PANEL_HPP

#include "PlotData.hpp"

#include <wx/wx.h>
#include <mgl2/mgl.h>

#include <vector>

class MathGLPanel : public wxPanel {
  private:
    mglGraph graph;
    PlotData plot_data;

  public:
    MathGLPanel(wxWindow* parent);

    /** Clears the data and refreshes the panel. */
    void clear();

    /** Adds new data data to the panel and refreshes the panel. */
    void add_point(std::array<double, 2> point);

  private:
    /** Called by the framework when the panel needs to be redrawn. */
    void on_paint_event(wxPaintEvent& event);

    /** Uses plotting library to create graph with the provided size. */
    std::vector<unsigned char> create_graph(unsigned width, unsigned height);

    DECLARE_EVENT_TABLE()
};

#endif
