#include "evaluation/EvaluationManager.hpp"

#include "evaluation/ConfigurationLoader.hpp"

namespace Evaluation {

/** Static configuration object. */
namespace {
McDeval::Configuration configuration;
bool configuration_loaded = false;
}

void load_configuration(const std::filesystem::path& filename)
{
    configuration = ConfigurationLoader::load(filename);
    configuration.validate();
    configuration_loaded = true;
}

const McDeval::Configuration& get_configuration()
{
    if (!configuration_loaded) {
        throw std::logic_error("Configuration accessed before loading.");
    }
    return configuration;
}

}
