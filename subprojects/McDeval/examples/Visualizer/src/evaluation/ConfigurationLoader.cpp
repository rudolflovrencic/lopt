#include "evaluation/ConfigurationLoader.hpp"

#include <rapidxml/rapidxml.hpp>

#include <fstream>
#include <string>
#include <sstream>
#include <unordered_set>
#include <algorithm>

namespace {
std::vector<char> load_file_data(const std::filesystem::path& filename);

void extract_resources_node(rapidxml::xml_document<>& dc,
                            McDeval::Configuration& cfg);
void extract_fragment_node(rapidxml::xml_node<>* f,
                           McDeval::Configuration& cfg);
void extract_result_node(rapidxml::xml_node<>* r, McDeval::Configuration& cfg);

void extract_application_node(rapidxml::xml_document<>& dc,
                              McDeval::Configuration& cfg);
void extract_component_node(rapidxml::xml_node<>* c,
                            McDeval::Configuration& cfg);
std::vector<std::string>
load_component_inputs(rapidxml::xml_node<>* c,
                      const McDeval::Configuration& cfg);
std::vector<std::string>
load_component_outputs(rapidxml::xml_node<>* c,
                       const McDeval::Configuration& cfg);

void set_resource_source_and_destination(rapidxml::xml_document<>& dc,
                                         McDeval::Configuration& cfg);
void set_resource_source_and_destination_for_component(
    rapidxml::xml_node<>* c, McDeval::Configuration& cfg);

void extract_deployment_node(rapidxml::xml_document<>& dc,
                             McDeval::Configuration& cfg);

void extract_security_node(rapidxml::xml_document<>& dc,
                           McDeval::Configuration& cfg);
std::pair<std::vector<const McDeval::Fragment*>,
          std::vector<const McDeval::Result*>>
load_constraint_resources(rapidxml::xml_node<>* c,
                          const McDeval::Configuration& cfg);

unsigned parse_unsigned(const std::string& val, const std::string& name);
void validate_id_attribute(const rapidxml::xml_attribute<>* id_attr,
                           const std::string& id_owner_name);
}

namespace Evaluation::ConfigurationLoader {
McDeval::Configuration load(const std::filesystem::path& filename)
{
    // Characters from the XML configuration file.
    std::vector<char> data = load_file_data(filename);

    rapidxml::xml_document<> doc;
    doc.parse<0>(&data[0]); // Parse XML document string with default settings.

    McDeval::Configuration config;
    extract_resources_node(doc, config);
    extract_application_node(doc, config);

    set_resource_source_and_destination(doc, config);

    extract_deployment_node(doc, config);
    extract_security_node(doc, config);

    return config;
}
}

namespace {

std::vector<char> load_file_data(const std::filesystem::path& filename)
{
    // Characters from the XML configuration file.
    std::vector<char> data;

    // Open stream.
    std::basic_ifstream<char> stream(filename, std::ios::binary);
    if (!stream) {
        std::stringstream ss;
        ss << "Cannot open file: " << filename;
        throw std::runtime_error(ss.str());
    }
    stream.unsetf(std::ios::skipws);

    // Determine stream size.
    stream.seekg(0, std::ios::end);
    size_t size = stream.tellg();
    stream.seekg(0);

    // Load data and add termination character.
    data.resize(size + 1);
    stream.read(&data.front(), static_cast<std::streamsize>(size));
    data[size] = '\0';

    return data;
}

void extract_resources_node(rapidxml::xml_document<>& dc,
                            McDeval::Configuration& cfg)
{
    // Get the resources node.
    auto r = dc.first_node("resources");
    if (!r) throw std::logic_error("No resources node in configuration file.");

    // Get the first fragment and make sure at least one fragment is given.
    auto f = r->first_node("fragment");
    if (!f) {
        throw std::logic_error(
            "McDeval::Resources node contains no fragment nodes.");
    }
    for (; f; f = f->next_sibling("fragment")) { // Iterate over fragments.
        extract_fragment_node(f, cfg); // Load fragment to the configuration.
    }

    // Get the first result. No result nodes is acceptable.
    for (auto x = r->first_node("result"); x; x = x->next_sibling("result")) {
        extract_result_node(x, cfg); // Load result to the configuration.
    }
}

void extract_fragment_node(rapidxml::xml_node<>* f, McDeval::Configuration& cfg)
{
    rapidxml::xml_attribute<>* fid = f->first_attribute("ID");
    validate_id_attribute(fid, "McDeval::Fragment");

    // Get fragment importance.
    auto imn = f->first_node("importance");
    if (!imn) {
        throw std::logic_error("McDeval::Fragment node must contain importance "
                               "node.");
    }
    unsigned importance = parse_unsigned(imn->value(), "fragment importance");

    // Create fragment with provided ID and importance.
    cfg.fragments.emplace_back(fid->value(), importance);
}

void extract_result_node(rapidxml::xml_node<>* res, McDeval::Configuration& cfg)
{
    rapidxml::xml_attribute<>* rid = res->first_attribute("ID");
    validate_id_attribute(rid, "McDeval::Result");

    // Get fragment importance.
    auto imn = res->first_node("importance");
    if (!imn) {
        throw std::logic_error(
            "McDeval::Result node must contain importance node.");
    }
    unsigned importance = parse_unsigned(imn->value(), "result importance");

    // Create result with provided ID and importance.
    cfg.results.emplace_back(rid->value(), importance);
}

void extract_application_node(rapidxml::xml_document<>& dc,
                              McDeval::Configuration& cfg)
{
    // Get the application node.
    auto a = dc.first_node("application");
    if (!a) {
        throw std::logic_error("No application node in configuration file.");
    }

    // Get first component node.
    auto c = a->first_node("component");
    if (!c) {
        throw std::logic_error("Application node must contain at least one "
                               "component node.");
    }
    for (; c; c = c->next_sibling("component")) { // Component nodes.
        extract_component_node(c, cfg); // Load component to configuration.
    }
}

void extract_component_node(rapidxml::xml_node<>* c,
                            McDeval::Configuration& cfg)
{
    // Load component ID.
    rapidxml::xml_attribute<>* cid = c->first_attribute("ID");
    validate_id_attribute(cid, "Component");

    std::vector<const McDeval::Resource*>
        inputs; // Pointers to input resources.
    std::vector<std::string> input_ids = load_component_inputs(c, cfg);
    {
        // IDs of accessed resources. Component inputs.
        inputs.reserve(input_ids.size());

        { // Create pointers to fragment objects from loaded fragment IDs.
            std::vector<McDeval::Fragment>& fs =
                cfg.fragments; // Shortcut reference.
            for (auto&& iid : input_ids) {
                auto iter = std::find(fs.begin(), fs.end(), iid);
                if (iter != fs.end()) inputs.push_back(&*iter);
            }
        }
        { // Create pointers to result objects from loaded result IDs.
            std::vector<McDeval::Result>& rs =
                cfg.results; // Shortcut reference.
            for (auto&& iid : input_ids) {
                auto iter = std::find(rs.begin(), rs.end(), iid);
                if (iter != rs.end()) inputs.push_back(&*iter);
            }
        }
    }

    std::vector<const McDeval::Result*> outputs; // Pointers to output results.
    std::vector<std::string> output_ids = load_component_outputs(c, cfg);
    {
        // IDs of output results. Component outputs.
        outputs.reserve(output_ids.size());

        { // Create pointers to fragment objects from loaded fragment IDs.
            std::vector<McDeval::Result>& rs =
                cfg.results; // Shortcut reference.
            for (auto&& oid : output_ids) {
                auto iter = std::find(rs.begin(), rs.end(), oid);
                if (iter != rs.end()) outputs.push_back(&*iter);
            }
        }
    }

    // Make sure that component does not have same resource as input and output.
    for (auto&& input_id : input_ids) {
        if (std::find(output_ids.begin(), output_ids.end(), input_id) !=
            output_ids.end()) { // ID is both in both outputs and inputs.
            throw std::runtime_error(
                "McDeval::Resource cannot be both input and "
                "output of a single component.");
        }
    }

    // Create component from ID and pointers to accessed fragments.
    cfg.components.emplace_back(cid->value(), inputs, outputs);
}

std::vector<std::string>
load_component_inputs(rapidxml::xml_node<>* c,
                      const McDeval::Configuration& cfg)
{
    std::vector<std::string> inputs; // Inputs to this component. Can be empty.

    for (auto i = c->first_node("input"); i; i = i->next_sibling("input")) {
        inputs.push_back(i->value());
    }

    // Check if all resource ID in input list are valid.
    const std::vector<McDeval::Fragment>& fs =
        cfg.fragments;                                    // Shortcut reference.
    const std::vector<McDeval::Result>& rs = cfg.results; // Shortcut reference.
    for (auto&& iid : inputs) { // Iterate over input IDs.
        // Check if input with given ID exists in fragments or results vector.
        if (std::find(fs.begin(), fs.end(), iid) == fs.end() &&
            std::find(rs.begin(), rs.end(), iid) == rs.end()) {
            throw std::runtime_error("Component references unknown resource as "
                                     "input.");
        }
    }

    // Make sure all IDs of accessed resources are unique. Use unordered set to
    // kill duplicate IDs.
    std::unordered_set<std::string> unique_input_ids(inputs.begin(),
                                                     inputs.end());
    if (unique_input_ids.size() != inputs.size()) {
        throw std::logic_error("Component must access each resource only "
                               "once.");
    }

    return inputs;
}

std::vector<std::string>
load_component_outputs(rapidxml::xml_node<>* c,
                       const McDeval::Configuration& cfg)
{
    std::vector<std::string>
        outputs; // Outputs of this component. Can be empty.
    for (auto o = c->first_node("output"); o; o = o->next_sibling("output")) {
        outputs.push_back(o->value());
    }

    // Check if all resource ID in output list are valid.
    const std::vector<McDeval::Result>& rs = cfg.results; // Shortcut reference.
    for (auto&& oid : outputs) { // Iterate over output IDs.
        // Check if output with given ID exists in results vector.
        if (std::find(rs.begin(), rs.end(), oid) == rs.end()) {
            std::stringstream ss;
            ss << "Component references unknown result (" << oid
               << ") as output.";
            throw std::runtime_error(ss.str());
        }
    }

    // Make sure all IDs of outputs are unique. Use unordered set to kill
    // duplicate IDs.
    std::unordered_set<std::string> unique_output_ids(outputs.begin(),
                                                      outputs.end());
    if (unique_output_ids.size() != outputs.size()) {
        throw std::logic_error("Component must reference each result as output "
                               "only once.");
    }

    return outputs;
}

void set_resource_source_and_destination(rapidxml::xml_document<>& dc,
                                         McDeval::Configuration& cfg)
{
    // Get the application node.
    auto a = dc.first_node("application");
    if (!a) {
        throw std::logic_error("No application node in configuration file.");
    }

    // Get first component node.
    auto c = a->first_node("component");
    if (!c) {
        throw std::logic_error("Application node must contain at least one "
                               "component node.");
    }
    for (; c; c = c->next_sibling("component")) { // Component nodes.
        set_resource_source_and_destination_for_component(c, cfg);
    }
}

void set_resource_source_and_destination_for_component(
    rapidxml::xml_node<>* c, McDeval::Configuration& cfg)
{
    // Load component ID.
    rapidxml::xml_attribute<>* cid = c->first_attribute("ID");
    validate_id_attribute(cid, "Component");

    // Find component with the provided ID in components vector.
    auto it =
        std::find(cfg.components.begin(), cfg.components.end(), cid->value());
    if (it == cfg.components.end()) {
        throw std::runtime_error("Component with provided ID could not be "
                                 "found in configuration object.");
    }
    const McDeval::Component& component = *it;

    { // Link fragments that are inputs to this component by adding a pointer to
      // this component to internal vector of a fragment.
        std::vector<std::string> input_ids = load_component_inputs(c, cfg);
        std::vector<McDeval::Fragment>& fs =
            cfg.fragments; // Shortcut reference.
        for (auto&& iid : input_ids) {
            auto iter = std::find(fs.begin(), fs.end(), iid);
            if (iter != fs.end()) iter->add_destination(component);
        }
    }

    { // Link results that are inputs to this component to this component by
        // setting the destination of the results.
        std::vector<std::string> input_ids = load_component_inputs(c, cfg);
        std::vector<McDeval::Result>& rs   = cfg.results; // Shortcut reference.
        for (auto&& iid : input_ids) {
            auto iter = std::find(rs.begin(), rs.end(), iid);
            if (iter != rs.end()) iter->set_destination(component);
        }
    }

    std::vector<McDeval::Result*> result_outputs;
    { // Link results that are outputs of this component to this component by
        // setting the source of the results.
        std::vector<std::string> output_ids = load_component_outputs(c, cfg);
        std::vector<McDeval::Result>& rs = cfg.results; // Shortcut reference.
        for (auto&& oid : output_ids) {
            auto iter = std::find(rs.begin(), rs.end(), oid);
            if (iter != rs.end()) iter->set_source(component);
        }
    }
}

void extract_deployment_node(rapidxml::xml_document<>& dc,
                             McDeval::Configuration& cfg)
{
    // Get the deployment node.
    auto a = dc.first_node("deployment");
    if (!a) {
        throw std::logic_error("No deployment node in configuration file.");
    }

    // Get first server node.
    auto s = a->first_node("server");
    if (!s) {
        throw std::logic_error("Deployment node must contain at least one "
                               "server node.");
    }
    for (; s; s = s->next_sibling("server")) { // Server nodes.
        // Get the ID of the server.
        rapidxml::xml_attribute<>* sid = s->first_attribute("ID");
        if (!sid) {
            throw std::logic_error("Server node must have ID attribute.");
        }
        if (sid->value_size() == 0) {
            throw std::logic_error("Server ID cannot be empty string.");
        }

        // Get the trust of the server.
        auto t = s->first_node("trust");
        if (!t) throw std::logic_error("Server node must contain trust node.");
        unsigned trust = parse_unsigned(t->value(), "server trust");

        // Create a server from configuration data.
        McDeval::Server ser(sid->value(), trust);

        // Make sure that the created server has not already been seen.
        if (std::find(cfg.servers.begin(), cfg.servers.end(), ser) !=
            cfg.servers.end()) {
            std::stringstream ss;
            ss << "Server '" << ser
               << "' is not unique. Consider assigning different ID.";
            throw std::runtime_error(ss.str());
        }

        // Push server to configuration vector.
        cfg.servers.push_back(ser);
    }
}

void extract_security_node(rapidxml::xml_document<>& dc,
                           McDeval::Configuration& cfg)
{
    // Get the deployment node.
    auto a = dc.first_node("security");
    if (!a) {
        throw std::logic_error("No security node in configuration file.");
    }

    // Get first constraint node.

    // It is acceptable that no constraints have been provided.
    for (auto c = a->first_node("constraint"); c;
         c      = c->next_sibling("constraint")) {
        // Get the ID of the constraint.
        rapidxml::xml_attribute<>* cid = c->first_attribute("ID");
        validate_id_attribute(cid, "Constraint");

        // Load constraint resources.
        auto res = load_constraint_resources(c, cfg);

        // Create a security constraint from fragments and results.
        cfg.constraints.emplace_back(cid->value(), res.first, res.second);
    }
}

std::pair<std::vector<const McDeval::Fragment*>,
          std::vector<const McDeval::Result*>>
load_constraint_resources(rapidxml::xml_node<>* c,
                          const McDeval::Configuration& cfg)
{
    // Resources of this constraint.
    std::pair<std::vector<const McDeval::Fragment*>,
              std::vector<const McDeval::Result*>>
        res;

    for (auto cf = c->first_node("resource"); cf;
         cf      = cf->next_sibling("resource")) {
        // Find resource in the fragments.
        auto it =
            std::find(cfg.fragments.begin(), cfg.fragments.end(), cf->value());
        if (it !=
            cfg.fragments.end()) { // McDeval::Resource found in fragments.
            res.first.push_back(&*it);
        } else { // Try to look into results for given resource.
            auto it =
                std::find(cfg.results.begin(), cfg.results.end(), cf->value());
            if (it !=
                cfg.results.end()) { // McDeval::Resource found in results.
                res.second.push_back(&*it);
            } else {
                throw std::runtime_error("Constraint references unknown "
                                         "resource.");
            }
        }
    }

    return res;
}

unsigned parse_unsigned(const std::string& val, const std::string& name)
{
    // Convert number string to a number.
    try {
        return std::stoul(val);
    } catch (const std::logic_error&) { // Failed to convert string to unsigned.
        throw std::logic_error("Given " + name +
                               " is not a valid unsigned "
                               "integer value.");
    }
}

void validate_id_attribute(const rapidxml::xml_attribute<>* id_attr,
                           const std::string& id_owner_name)
{
    if (!id_attr) {
        std::stringstream ss;
        ss << id_owner_name << " node must have ID attribute.";
        throw std::logic_error(ss.str());
    }
    if (id_attr->value_size() == 0) {
        std::stringstream ss;
        ss << id_owner_name << " ID cannot be empty string.";
        throw std::logic_error(ss.str());
    }
}

}
