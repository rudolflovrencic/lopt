#include "ControlsPanel.hpp"

#include "Identifiers.hpp"

ControlsPanel::ControlsPanel(wxWindow* parent)
    : wxPanel(parent),
      evaluate_button(new wxButton(this, ID::evaluate_button, "Evaluate")),
      clear_button(new wxButton(this, ID::clear_button, "Clear")),
      quit_button(new wxButton(this, ID::quit_button, "Quit"))
{
    constexpr int border_size = 10; // Number of pixels in the border (padding).

    auto sizer = new wxBoxSizer(wxHORIZONTAL);
    sizer->AddStretchSpacer(); // Pushes buttons to the left.
    sizer->Add(evaluate_button, 0, wxTOP | wxLEFT | wxBOTTOM, border_size);
    sizer->Add(clear_button, 0, wxTOP | wxLEFT | wxBOTTOM, border_size);
    sizer->Add(quit_button, 0, wxALL, border_size);
    SetSizer(sizer);
}
