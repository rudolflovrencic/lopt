#include "MainFrame.hpp"

#include "Identifiers.hpp"
#include "AllocatablePanel.hpp"
#include "ControlsPanel.hpp"
#include "McDeval/Evaluator.hpp"
#include "evaluation/EvaluationManager.hpp"

MainFrame::MainFrame()
    : wxFrame(nullptr, wxID_ANY, "Visualizer", {30, 30}, {800, 600}),
      servers_panel(new ServersPanel(this)),
      right_panel(new wxPanel(this))
{
    right_panel->SetMinSize({650, -1}); // Set minimum width.
    {                                   // Set the sizer.
        auto sizer = new wxBoxSizer(wxHORIZONTAL);
        sizer->Add(servers_panel, 3, wxEXPAND);
        sizer->Add(right_panel, 4, wxEXPAND);
        SetSizer(sizer);
    }
    { // Right panel.
        auto sizer = new wxBoxSizer(wxVERTICAL);
        { // Top part of the right panel. Contains two collection panels.
            auto top_right_panel = new wxPanel(right_panel);
            sizer->Add(top_right_panel, 0, wxEXPAND);
            // REMOVE
            top_right_panel->SetSizer(new wxBoxSizer(wxVERTICAL));
            top_right_panel->GetSizer()->AddSpacer(10);
            { // First collection panel.
                fragments_panel = new CollectionPanel(top_right_panel);
                fragments_panel->SetMinSize({-1, 54});
                for (auto& f : Evaluation::get_configuration().fragments) {
                    if (f.id.length() != 2) {
                        std::stringstream ss;
                        ss << "Only fragments with ID length equal to two are "
                           << "supported. Problematic fragment ID: " << f.id;
                        throw std::runtime_error(ss.str());
                    }
                    fragments_panel->add(
                        new FragmentPanel(fragments_panel, f.id[1]));
                }
                fragments_panel->GetSizer()->AddSpacer(10);
                top_right_panel->GetSizer()->Add(
                    fragments_panel, 0, wxEXPAND | wxRIGHT, 10);
            }
            top_right_panel->GetSizer()->AddSpacer(10);
            { // Second collection panel.
                components_panel = new CollectionPanel(top_right_panel);
                components_panel->SetMinSize({-1, 54});
                for (auto& c : Evaluation::get_configuration().components) {
                    if (c.id.length() != 2) {
                        std::stringstream ss;
                        ss << "Only components with ID length equal to two are "
                           << "supported. Problematic component ID: " << c.id;
                        throw std::runtime_error(ss.str());
                    }
                    components_panel->add(
                        new ComponentPanel(components_panel, c.id[1]));
                }
                components_panel->GetSizer()->AddSpacer(10);
                top_right_panel->GetSizer()->Add(
                    components_panel, 0, wxEXPAND | wxRIGHT | 10);
            }
            top_right_panel->GetSizer()->AddSpacer(10);
        }
        {
            math_gl_panel = new MathGLPanel(right_panel);
            sizer->Add(math_gl_panel, 1, wxEXPAND | wxALL, 10);
        }
        {
            auto controls_panel = new ControlsPanel(right_panel);
            sizer->Add(controls_panel, 0, wxEXPAND);
        }
        right_panel->SetSizer(sizer);
    }

    GetSizer()->Fit(this);
}

void MainFrame::on_quit_button_pressed(wxCommandEvent& /*event*/)
{
    Close();
}

void MainFrame::on_clear_button_pressed(wxCommandEvent& /*event*/)
{
    math_gl_panel->clear();
}

void MainFrame::on_evaluate_button_pressed(wxCommandEvent& /*event*/)
{
    // Make sure all fragments and components are assigned to a server.
    if (!fragments_panel->empty() || !components_panel->empty()) {
        auto message_dialog =
            wxMessageDialog(this,
                            "Evaluation failed",
                            wxMessageBoxCaptionStr,
                            wxOK | wxOK_DEFAULT | wxICON_ERROR | wxCENTRE);
        message_dialog.SetExtendedMessage(
            "All fragments and components must be assigned to a server before "
            "evaluating the solution.");
        message_dialog.ShowModal();
        return;
    }

    // Construct string representation of solution based on users drag and drop
    // actions. String representation is used to construct solution object.
    std::stringstream sol_as_string;
    for (auto& server : *servers_panel) {
        auto& server_panel = dynamic_cast<ServerPanel&>(server);
        sol_as_string << 'S' << server_panel.index << ": ";
        auto names = server_panel.get_assigned_allocatable_names();
        for (auto& name : names) sol_as_string << name[0] << name[1] << ' ';
        sol_as_string << '\n';
    }

    // Fetch configuration object reference.
    auto& configuration = Evaluation::get_configuration();

    // Construct evaluators.
    auto constraint_evaluator = McDeval::Evaluator::create_evaluator(
        McDeval::Evaluator::Type::Constraint, configuration);
    auto single_resource_evaluator = McDeval::Evaluator::create_evaluator(
        McDeval::Evaluator::Type::SingleResource, configuration);

    // Construct solution object.
    McDeval::Solution solution(sol_as_string.str(), configuration);

    // Evaluate the solution.
    auto constraint_risk      = constraint_evaluator->evaluate(solution);
    auto single_resource_risk = single_resource_evaluator->evaluate(solution);

    // Draw the point.
    math_gl_panel->add_point(
        {single_resource_risk.get_value(), constraint_risk.get_value()});

    std::cout << "----- Evaluate button pressed! -----\n"
              << "Soltuion:\n"
              << solution << "Single resource risk: " << single_resource_risk
              << "\nConstraint risk:      " << constraint_risk
              << "\n------------------------------------" << std::endl;
}

wxBEGIN_EVENT_TABLE(MainFrame, wxFrame) // clang-format off
    EVT_BUTTON(ID::quit_button, MainFrame::on_quit_button_pressed)
    EVT_BUTTON(ID::clear_button, MainFrame::on_clear_button_pressed)
    EVT_BUTTON(ID::evaluate_button, MainFrame::on_evaluate_button_pressed)
wxEND_EVENT_TABLE() // clang-format on
