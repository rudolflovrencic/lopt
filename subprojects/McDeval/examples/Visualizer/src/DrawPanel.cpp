#include "DrawPanel.hpp"

DrawPanel::DrawPanel(wxWindow* parent) : wxPanel(parent) {}

void DrawPanel::on_paint_event(wxPaintEvent& /*event*/)
{
    // Create a painting context for this panel. Enables drawing to the panel.
    wxPaintDC paint_device_context(this);
    render(paint_device_context); // Render using the device context.
}

wxBEGIN_EVENT_TABLE(DrawPanel, wxPanel) // clang-format off
    EVT_PAINT(DrawPanel::on_paint_event)
wxEND_EVENT_TABLE() // clang-format on
