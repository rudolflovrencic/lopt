#include "Application.hpp"

#include "evaluation/EvaluationManager.hpp"

bool Application::OnInit()
{
    // Load static configuration object so this 
    Evaluation::load_configuration("res/configuration/config.xml");

    main_frame = new MainFrame();
    main_frame->Show();

    return true;
}

bool Application::OnExceptionInMainLoop()
{
    try {
        throw; // Re-throw current exception.
    } catch (std::exception& e) {
        std::cout << "Exception has occurred in the main loop!\n"
                  << e.what() << "\nNow exiting..." << std::endl;
    } catch (...) {
        std::cout << "Unexpected exception has been thrown. Now exiting..."
                  << std::endl;
    }
    return false; // Do not ignore the exception. Exit the main loop.
}

void Application::OnUnhandledException()
{
    try {
        throw; // Re-throw current exception.
    } catch (std::exception& e) {
        std::cout << "Unhandled exception has occurred outside the main loop!\n"
                  << e.what() << "\nNow exiting..." << std::endl;
    } catch (...) {
        std::cout << "Unexpected exception has been thrown. Now exiting..."
                  << std::endl;
    }
}

// Generate entry point.
wxIMPLEMENT_APP(Application);
