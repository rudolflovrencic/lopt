#ifndef MCDEVAL_SOLUTION_HPP
#define MCDEVAL_SOLUTION_HPP

#include <unordered_map>

#include "McDeval/Component.hpp"
#include "McDeval/Fragment.hpp"
#include "McDeval/Server.hpp"
#include "McDeval/Configuration.hpp"
#include "McDeval/Hasher.hpp"

namespace McDeval {

class Solution {
  private:
    /** Mapping of data fragments to the servers. Both key and value are
     * pointers to configuration objects so it needs to be ensured that part of
     * configuration object that stores fragments and servers remains unchanged
     * for the duration of this object usage. */
    std::unordered_map<const Fragment*, const Server*> fragment_mapping;

    /** Mapping of data components to the servers. Both key and value are
     * pointers to configuration objects so it needs to be ensured that part of
     * configuration object that stores components and servers remains unchanged
     * for the duration of this object usage. */
    std::unordered_map<const Component*, const Server*> component_mapping;

  public:
    /** Creates empty solution. */
    Solution();

    /** Constructs a solution based on the string passed in. For example, a
     * solution where two fragments F1 and F2 are stored on the server S0,
     * component C0 is running on server S1 and F0 is stored on the server S1 as
     * well is defined as:
     *          S0: F1 F2
     *          S1: C0 F0
     * Ordering of servers, components and fragments is arbitrary. If some
     * server does not host and components or fragments, it should be left out.
     * Colon after server name is mandatory.
     */
    Solution(const std::string& sol_as_string, const Configuration& config);

    /** Sets server of assignable object. If the assignable object does not yet
     * exist in the solution, it is added. Only template specializations of this
     * functions should be used. */
    template<typename T>
    void set_server(const T& assignable, const Server& server);

    /** Returns a server where given assignable object is assigned. Only
     * template specializations of this function should be used. */
    template<typename T>
    const Server& get_server(const T& assignable) const;

    bool operator==(const Solution& other) const;

    friend std::ostream& operator<<(std::ostream& ostr,
                                    const Solution& solution);

    friend size_t std::hash<Solution>::operator()(const Solution& s) const;

    /** Constructs a McDeval::Solution based on the string passed in. For
     * example, solution where two fragments F1 and F2 are stored on the server
     * S0, component C0 is running on server S1 and F0 is stored on the server
     * S1 as well is defined as:
     *          S0: F1 F2
     *          S1: C0 F0
     * Ordering of servers, components and fragments is arbitrary. If some
     * server does not host and components or fragments, it should be left out.
     * Colon after server name is mandatory.
     */
    // static Solution create(const std::string& sol_as_string,
    // const Configuration& config);
};

}

#endif
