#ifndef MCDEVAL_COMPONENT_HPP
#define MCDEVAL_COMPONENT_HPP

#include <vector>
#include <ostream>

#include "McDeval/Resource.hpp"
#include "McDeval/Result.hpp"
#include "McDeval/Hasher.hpp"

namespace McDeval {

class Component {
  public:
    /** Unique component identifier. */
    const std::string id;

  private:
    /** Accessed data fragments. All pointers are non-owning and are guaranteed
     * to not be nullptr. */
    std::vector<const Resource*> inputs;

    /** Created results by this component that other components can use. All
     * pointers are non-owning and are guaranteed to not be nullptr. */
    std::vector<const Result*> outputs;

  public:
    Component(std::string id,
              const std::vector<const Resource*>& inputs,
              const std::vector<const Result*>& outputs);

    std::vector<const Resource*>::const_iterator inputs_begin() const;
    std::vector<const Resource*>::const_iterator inputs_end() const;
    std::vector<const Result*>::const_iterator outputs_begin() const;
    std::vector<const Result*>::const_iterator outputs_end() const;

    bool operator==(const Component& other) const;
    bool operator==(const std::string& other) const;

    friend std::ostream& operator<<(std::ostream& s, const Component& c);

    /** Makes this object hashable so it can be used as a key in collections
     * that require hashable keys. */
    friend size_t std::hash<McDeval::Component>::operator()(
        const McDeval::Component& c) const;
};

}

#endif
