#ifndef MCDEVAL_CONSTRAINT_HPP
#define MCDEVAL_CONSTRAINT_HPP

#include <vector>
#include <ostream>

#include "McDeval/Server.hpp"
#include "McDeval/Fragment.hpp"
#include "McDeval/Result.hpp"
#include "McDeval/Hasher.hpp"

namespace McDeval {

class Solution;

/** Security constraint defined on application resources. Resources that are
 * considered when determining the security risk are data fragments and results
 * that are sent between and used by application components. */
class Constraint {
  private:
    /** Unique constraint identifier. */
    std::string id;

    /** Fragments in the constraint. All pointers are non-owning and guaranteed
     * to not be nullptr. Pointers point to objects in configuration object so
     * for during the lifetime of this object it should be guaranteed that part
     * of configuration object that stores fragments will not change. */
    std::vector<const Fragment*> fragments;

    /** Results in the constraint. All pointers are non-owning and guaranteed
     * to not be nullptr. Pointers point to objects in configuration object so
     * for during the lifetime of this object it should be guaranteed that part
     * of configuration object that stores results will not change. */
    std::vector<const Result*> results;

  public:
    Constraint(std::string id,
               const std::vector<const Fragment*>& fragments,
               const std::vector<const Result*>& results);

    /** Returns a set of servers that break this constraint. If returned set is
     * empty, the constraint is not broken (it is fulfiled). */
    ServerSet get_breaking_servers(const Solution& sol) const;

    /** Checks weather or not provided solution fulfills this constraint. */
    bool is_fulfiled(const Solution& solution) const;

    /** Checks weather or not provided solution breaks this constraint. */
    bool is_broken(const Solution& solution) const;

    /** Checks weather or not this constraint contains same (and only the same)
     * resources as other. In other words, number of resources in both
     * constraints has to be the same before check if done if all fragments are
     * the same. */
    bool contains_same_resources_as(const Constraint& other) const;

    bool operator==(const Constraint& other) const;
    bool operator!=(const Constraint& other) const;

    friend std::ostream& operator<<(std::ostream& s, const Constraint& c);

    /** Makes this object hashable so it can be used as a key in collections
     * that require hashable keys. */
    friend size_t std::hash<Constraint>::operator()(const Constraint& c) const;
};

}

#endif
