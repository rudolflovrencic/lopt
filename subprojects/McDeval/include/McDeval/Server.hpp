#ifndef MCDEVAL_SERVER_HPP
#define MCDEVAL_SERVER_HPP

#include <string>
#include <ostream>

#include "McDeval/Hasher.hpp"

namespace McDeval {

class Server {
  public:
    /** Unique identifier of the server. */
    const std::string id;

    /** Trust value of the server. The higher the value, the lower the risk of
     * this server compromising the data. */
    const unsigned trust;

  public:
    Server(std::string id, unsigned trust);

    unsigned get_trust() const;

    bool operator==(const Server& other) const;
    bool operator==(const std::string& other) const;

    bool operator!=(const Server& other) const;

    /** Used for sorting servers by ID and inserting into sets. */
    bool operator<(const Server& other) const;

    friend std::ostream& operator<<(std::ostream& s, const Server& server);

    /** Makes this object hashable so it can be used as a key in collections
     * that require hashable keys. */
    friend size_t std::hash<Server>::operator()(const Server& res) const;
};

}

#endif
