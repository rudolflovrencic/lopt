#ifndef MCDEVAL_SERVERSET_HPP
#define MCDEVAL_SERVERSET_HPP

#include <set>

#include "McDeval/Server.hpp"

namespace McDeval {

/** Used for comparing server pointers with less operator of server class
 * (semantically, not by addresses). */
class ServerPointerCompare {
  public:
    bool operator()(const Server* a, const Server* b) const;
};

using ServerSetConstIterator =
    std::set<const Server*, ServerPointerCompare>::const_iterator;

class ServerSet {
  private:
    std::set<const Server*, ServerPointerCompare> servers;

  public:
    explicit ServerSet(const std::set<const Server*>& servers);

    void inplace_intersect(const ServerSet& other);

    const Server& get_most_insecure_server() const;

    bool empty() const;

    ServerSetConstIterator begin() const;
    ServerSetConstIterator end() const;

    bool operator==(const ServerSet& other) const;

    friend std::ostream& operator<<(std::ostream& s, const ServerSet& sr);
};

}

#endif
