#ifndef MCDEVAL_RESULT_HPP
#define MCDEVAL_RESULT_HPP

#include "McDeval/Resource.hpp"

namespace McDeval {

class Component;

class Result : public Resource {
  private:
    const Component* source;
    const Component* destination;

  public:
    Result(const std::string& id, unsigned importance);

    void set_source(const Component& source);
    const Component& get_source() const;

    void set_destination(const Component& destination);
    const Component& get_destination() const;

    ServerSet server_reach(const Solution& s) const override;

    bool source_set() const;
    bool destination_set() const;
};

}

#endif
