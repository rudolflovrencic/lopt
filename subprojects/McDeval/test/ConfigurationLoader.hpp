#ifndef CONFIGURATION_LOADER_HPP
#define CONFIGURATION_LOADER_HPP

#include "McDeval.hpp"

#include <filesystem>

class ConfigurationLoader {
  public:
    /** Creates application configuration object based on configuration file. */
    static McDeval::Configuration load(const std::filesystem::path& filename);
};

#endif
