#include <catch2/catch.hpp>

#include "McDeval.hpp"

#include "ConfigurationLoader.hpp"
#include "SolutionConstructor.hpp"

TEST_CASE("Server reach")
{
    // Load and validate the configuration.
    const McDeval::Configuration config =
        ConfigurationLoader::load("res/app_config_1.xml");
    config.validate();

    SECTION("First solution")
    {
        const McDeval::Solution solution =
            SolutionConstructor::construct_solution("S0: F0 C0\n"
                                                    "S1: F1\n"
                                                    "S2: F2\n"
                                                    "S3: C1 C2",
                                                    config);

        // Expect resource server reaches to be as following:
        //      F0:  S0
        //      F1:  S1 S3
        //      F2:  S2 S3
        //      R12: S3

        SECTION("First resource")
        {
            McDeval::ServerSet expected_server_reach({&config.servers[0]});
            auto calculated_reach = config.fragments[0].server_reach(solution);
            REQUIRE(expected_server_reach == calculated_reach);
        }

        SECTION("Second resource")
        {
            McDeval::ServerSet expected_server_reach(
                {&config.servers[1], &config.servers[3]});
            auto calculated_reach = config.fragments[1].server_reach(solution);
            REQUIRE(expected_server_reach == calculated_reach);
        }

        SECTION("Third resource")
        {
            McDeval::ServerSet expected_server_reach(
                {&config.servers[2], &config.servers[3]});
            auto calculated_reach = config.fragments[2].server_reach(solution);
            REQUIRE(expected_server_reach == calculated_reach);
        }

        SECTION("Fourth resource")
        {
            McDeval::ServerSet expected_server_reach({&config.servers[3]});
            auto calculated_reach = config.results[0].server_reach(solution);
            REQUIRE(expected_server_reach == calculated_reach);
        }
    }

    SECTION("Second solution")
    {
        const McDeval::Solution solution =
            SolutionConstructor::construct_solution("S2: C0 C1 C2\n"
                                                    "S3: F0 F1 F2",
                                                    config);

        // Expect resource server reaches to be as following:
        //      F0:  S2 S3
        //      F1:  S2 S3
        //      F2:  S2 S3
        //      R12: S2

        SECTION("First resource")
        {
            McDeval::ServerSet expected_server_reach(
                {&config.servers[2], &config.servers[3]});
            auto calculated_reach = config.fragments[0].server_reach(solution);
            REQUIRE(expected_server_reach == calculated_reach);
        }

        SECTION("Second resource")
        {
            McDeval::ServerSet expected_server_reach(
                {&config.servers[2], &config.servers[3]});
            auto calculated_reach = config.fragments[1].server_reach(solution);
            REQUIRE(expected_server_reach == calculated_reach);
        }

        SECTION("Third resource")
        {
            McDeval::ServerSet expected_server_reach(
                {&config.servers[2], &config.servers[3]});
            auto calculated_reach = config.fragments[2].server_reach(solution);
            REQUIRE(expected_server_reach == calculated_reach);
        }

        SECTION("Fourth resource")
        {
            McDeval::ServerSet expected_server_reach({&config.servers[2]});
            auto calculated_reach = config.results[0].server_reach(solution);
            REQUIRE(expected_server_reach == calculated_reach);
        }
    }
}
