#ifndef SOLUTION_CONSTRUCTOR
#define SOLUTION_CONSTRUCTOR

#include "McDeval.hpp"

class SolutionConstructor {
  public:
    /** Constructs a McDeval::Solution based on the string passed in. For
     * example, solution where two fragments F1 and F2 are stored on the server
     * S0, component C0 is running on server S1 and F0 is stored on the server
     * S1 as well is defined as:
     *          S0: F1 F2
     *          S1: C0 F0
     * Ordering of servers, components and fragments is arbitrary. If some
     * server does not host and components or fragments, it should be left out.
     * Colon after server name is mandatory. */
    static McDeval::Solution
    construct_solution(const std::string& sol_as_string,
                       const McDeval::Configuration& config);
};

#endif
